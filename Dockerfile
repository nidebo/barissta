FROM node:8.9.1

RUN apt-get update
RUN apt-get -y install rubygems
RUN apt-get -y install ruby2.1-dev
RUN gem install -N mailcatcher

COPY package.json /tmp/package.json
RUN cd /tmp && yarn install
RUN mkdir -p /app && cp -a /tmp/node_modules /app

WORKDIR /app
COPY . /app

EXPOSE 3001 1080

CMD mailcatcher --ip=0.0.0.0 && npm run dev
