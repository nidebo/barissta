import assert from 'assert'
import User from './../src/User/model'

describe('Creating records', () => {
  it('saves a user', (done) => {
    // Create a new user
    const joe = new User({ firstName: 'joe', email: 'joe@gmail.com', password: 'pass1234' });

    // Save it to the database
    joe.save()
      .then(doc => {
        // Is Joe been saved successfully?
        assert(!joe.isNew);
        done();
      })
      .catch(error => {
        console.error('Error saving a user: ', error);
        done();
      });

    // Checking if it is indeed in the database

  });
});