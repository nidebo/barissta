import mongoose from 'mongoose'
mongoose.Promise = global.Promise;

before(done => {
  require('dotenv/config')
  mongoose.connect('mongodb://mongo:27017/barissta-test')
  const db = mongoose.connection
  db.on('error', (err) => {
    done(err)
  })
  db.once('open', () => {
    done()
  })
})

const removeUsers = async () => {
  const usersCollection = mongoose.connection.collections['users']
  const users = await usersCollection.find()
  return usersCollection.deleteMany({})
}

const removePromoCodes = () => {
  const promoCodesCollection = mongoose.connection.collections['promocodes']
  return promoCodesCollection.deleteMany({})
}

const removeShops = () => {
  const shopsCollection = mongoose.connection.collections['shops']
  return shopsCollection.deleteMany({})
}

beforeEach(() => {
  return Promise.all([removeUsers(), removePromoCodes(), removeShops()])
  .catch(err => {
    throw new Error(err)
  })
});

after(() => {
  return Promise.all([removeUsers, removePromoCodes, removeShops])
  .then(() => {
    return mongoose.connection.close()
  })
});
