// npm packages
import assert from 'assert'

// models
import User from './../src/User/model'

describe('Updating records', () => {
  let joe;

  beforeEach((done) => {
    joe = new User({ firstName: 'joe', email: 'joe@gmail.com', password: 'pass1234' });
    joe.save()
      .then(() => done())
      .catch(error => console.error('Error reading: ', error));
  });

  it('instance type using set and save', (done) => {
    joe.set('name', 'Alex');
    joe.save()
      .then(() => User.find({}))
      .then(users => {
        assert(users.length === 1);
        assert(users[0].name === 'Alex');
        done();
      })
      .catch(error => {
        console.error('Error updating: ', error);
        done();
      });
  });
});