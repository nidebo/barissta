// npm packages
import assert from 'assert'

// models
import User from './../src/User/model'

describe('Deleting a user', () => {
  let joe;

  beforeEach((done) => {
    joe = new User({ firstName: 'joe', email: 'joe@gmail.com', password: 'pass1234' });
    joe.save()
      .then(() => done())
      .catch(error => console.error('Error reading: ', error));
  });

  it('model instance remove', (done) => {
    joe.remove()
      .then(() => User.findOne({ firstName: 'joe' }))
      .then(user => {
        assert(user === null);
        done();
      })
      .catch(error => {
        console.error('Error reading: ', error);
        done();
      });
  });

  it('class method remove', (done) => {
    User.remove({ firstName: 'joe' })
      .then(() => User.findOne({ firstName: 'joe' }))
      .then(user => {
        assert(user === null);
        done();
      })
      .catch(error => {
        console.error('Error reading: ', error);
        done();
      });
  });

  it('class method findAndRemove', (done) => {
    User.findOneAndRemove({ firstName: 'joe' })
      .then(() => User.findOne({ firstName: 'joe' }))
      .then(user => {
        assert(user === null);
        done();
      })
      .catch(error => {
        console.error('Error reading: ', error);
        done();
      });
  });

  it('class method findByIdAndRemove', (done) => {
    User.findByIdAndRemove(joe._id)
      .then(() => User.findOne({ firstName: 'joe' }))
      .then(user => {
        assert(user === null);
        done();
      })
      .catch(error => {
        console.error('Error reading: ', error);
        done();
      })
  });
});