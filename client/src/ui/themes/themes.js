const mainTheme = {
  textMainColor: 'black',
  textSecondaryColor: 'white',
  mainGreen: '#10ABA3',
};

export { mainTheme };