import styled from 'styled-components'

const TutorialScreen = styled.div`
  background-color: white;
  border-radius: 4px;
  height: 100%;
  width: 100%;
  background-image: linear-gradient(135deg, #0E5CAD 10%, #79F1A4 100%);
  opacity: 0.98;
  position: fixed;
  z-index: 1;
  flex: 1 1 100%;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
`

const TutorialContent = styled.div`
  height: 90%;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
`

const TutorialDescription = styled.div`
  color: white;
  font-weight: bold;
  font-size: 20px;
  width: 230px;
  text-align: center;
`

const TutorialWelcome = styled.div`
  background-color: black;
  color: white;
  font-weight: bold;
  font-size: 20px;
  text-align: center;
  padding: 10px;
  width: 270px;
  margin: 0 auto;
`

const Button = styled.div`
  color: white;
  font-weight: bold;
  border: 1px solid white;
  width: 280px;
  font-size: 20px;
  height: 50px;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: rgba(255, 251, 251, 0.16);
`

const TutorialStep = styled.div`
  color: white;
  font-size: 18px;
  width: 250px;
  text-align: center;
  margin-bottom: 5px;
  font-weight: bold;
`

export {
  TutorialScreen,
  TutorialWelcome,
  Button,
  TutorialDescription,
  TutorialStep,
  TutorialContent
}
