import styled from 'styled-components'

const HomeBlockContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  margin-bottom: 20px;
  justify-content: space-between;
  box-sizing: border-box;
  flex-wrap: wrap;
`;

const Title = styled.div`
  width: 100%;
  font-size: 25px;
  font-weight: bold;
  text-align: center;
  margin-bottom: 15px;
`;

const Text = styled.div`
  font-size: 24px;
  text-align: center;
  padding: 0 30px;
  margin-top: 25px;
  margin-bottom: 15px;
  font-weight: bold;
  line-height: 1.2;
`;

const Separator = styled.div`
  height: 1px;
  width: 45%;
  margin-top: 20px;
  background-color: black;
`;

const SeparatorBlock = styled.div`
  margin: 10px auto;
  width: 80%;
  display: flex;
  flex-direction: row;
  justify-content: center;

  &.negativeMargin {
    margin-top: -66px;

    @media screen and (min-width: 768px) {
      margin-top: -55px;
    }
  }
`;

const CupIcon = styled.img`
  height: 40px;
`;

const Highlighted = styled.span`
  color: ${props => props.theme.mainGreen};
`;

export {
  HomeBlockContainer,
  Title,
  Text,
  SeparatorBlock,
  Separator,
  Highlighted,
  CupIcon
}