import styled from 'styled-components'
import CoffeeShopImage from '../../components/Home/img/coffee_shop.png'

const Title = styled.div`
  color: white;
  font-size: 24px;
  padding: 5px 10px;
  background-color: black;
`

const WorkWithBarisstaContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background-image: url(${CoffeeShopImage});
  background-size: cover;
  height: 220px;
`

const WorkWithBarisstaButton = styled.div`
  width: 180px;
  padding: 10px 0;
  text-align: center;
  border: 2px solid white;
  color: white;
  background-color: rgba(143, 146, 152, 0.65);
  font-size: 20px;
`

export {
  Title,
  WorkWithBarisstaContainer,
  WorkWithBarisstaButton
}
