import styled from 'styled-components'

const Title = styled.div`
  font-size: 20px;
  text-align: center;
  line-height: 1.2;
  border-bottom: 2px solid ${props => props.theme.mainGreen};
  padding-bottom: 15px;
`

const SuggestShopButton = styled.div`
  padding: 10px;
  background-color: #383434;
  color: white;
  font-size: 20px;
  margin-bottom: 10px;
`

const SuggestShopContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  border: 2px solid ${props => props.theme.mainGreen};
  padding: 20px;
`

const Description = styled.div`
  font-family: Georgia, serif;
  font-style: italic;
  width: 300px;
  font-size: 14px;
  margin: 20px 0;
  text-align: center;
`

export {
  Title,
  SuggestShopContainer,
  Description,
  SuggestShopButton
}
