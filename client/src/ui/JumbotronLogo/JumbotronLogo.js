import styled from 'styled-components'
import backgroundImage from '../../components/Login/img/marilyn.jpg'

const JumbotronLogoContainer = styled.div`
  width: 100%;
  height: 322px;
  display: flex;
  flex-direction: column;
  color: white;
  background-image: url(${backgroundImage});
  background-size: cover;
  background-position: center center;

  @media screen and (max-width: 424px) {
    justify-content: center;
  }
`;

const Text = styled.div`
  padding: 10px;
  margin: 0 auto;
  margin-top: 85px;
  font-size: 20px;
  width: 280px;
  background-color: black;
  text-align: right;
`;

const ButtonsContainer = styled.div`
  margin-top: 20px;
`

const Button = styled.div`
  width: 280px;
  height: 50px;
  background-color: ${props => props.theme.mainGreen};
  color: white;
  border-radius: 2px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  font-size: 24px;
  margin: 0 auto;
  margin-bottom: 20px;
`;

export { 
  JumbotronLogoContainer,
  Text,
  Button,
  ButtonsContainer
};
