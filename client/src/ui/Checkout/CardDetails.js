import styled from 'styled-components'

const CardImage = styled.img`
  width: 50px;
`

const Card = styled.div`
  border-radius: 2px;
  border: 2px solid ${props => props.theme.mainGreen};
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 0 40px;
  font-family: 'Source Sans Pro', sans-serif;
  height: 140px;
  width: 250px;
  margin: 60px 0;
  font-weight: bold;
`

const CardNumber = styled.div`
  margin-bottom: 10px;
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
`

const CardDots = styled.span`
  font-size: 30px;
`

const CardInfo = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
`

const CardExpiration = styled.div`
  font-size: 18px;
`

const CardLast4 = styled.span`
  font-size: 18px;
`

export {
  CardImage,
  Card,
  CardNumber,
  CardInfo,
  CardLast4,
  CardDots,
  CardExpiration
}
