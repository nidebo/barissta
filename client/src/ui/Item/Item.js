import styled from 'styled-components';

const ItemBlock = styled.div`
  width: 90%;
  cursor: pointer;
  margin-bottom: 10px;
  border-bottom: 1px solid #bbb;
  min-height: 60px;
  display: flex;
  align-items: center;
  font-weight: bold;
  font-size: 16px;
`;

const ItemDescription = styled.div`
  font-family: 'Source Sans Pro', sans-serif;
  color: black;
  width: 100%;
  min-height: 45px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

const ItemPrice = styled.div`
  color: #848484;
  margin: 5px 0;
`

const ItemDelete = styled.i`
  color: #848484;
  font-size: 20px;
  margin-left: 16px;
`

export {
  ItemBlock,
  ItemDescription,
  ItemPrice,
  ItemDelete
}
