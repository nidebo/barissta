import styled from 'styled-components';

const CoffeeShopScreen = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const Header = styled.div`
  width: 100%;
  min-height: 170px;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  background: linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.6)), url(${props => props.imageUrl});
  background-size: cover;
  background-position: center center;
  margin-bottom: 10px;

  &.locked {
    justify-content: center;
    position: fixed;
    min-height: 50px;
    height: 50px;
    top: 50px;
    z-index: 1;
    overflow: hidden;
    box-shadow: 0 3px 9px #bbb;
  }
`;

const ShopName = styled.div`
  width: 100%;
  font-size: 2.7em;
  text-align: center;
  text-transform: capitalize;
  margin-bottom: 30px;
  color: white;
  transition: font-size 0.5s;

  &.locked {
    margin-bottom: 0;
    font-size: 30px;
    transition: font-size 0.3s;
  }
`;

const ShopLocation = styled.span`
  cursor: pointer;
  margin-bottom: 2px;
  text-decoration: underline;
`;

const CoffeeShopInfo = styled.div`
  height: 35px;
  font-family: 'Source Sans Pro', sans-serif;
  display: flex;
  justify-content: space-between;
  flex-direction: column;
  color: white;
  font-size: 14px;
  margin-left: 20px;
  margin-bottom: 10px;

  & * i {
    font-size: 12px;
    margin-right: 5px;
  }

  &.locked {
    display: none;
  }
`

export {
  CoffeeShopScreen,
  Header,
  ShopName,
  ShopLocation,
  CoffeeShopInfo
};
