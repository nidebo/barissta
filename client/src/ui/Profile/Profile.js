import styled from 'styled-components';
import backgroundImage from '../../components/Login/img/marilyn.jpg'

const ProfileScreen = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
`;

const Header = styled.div`
  background: linear-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0)), url(${backgroundImage});
  background-size: cover;
  background-position: center;
  width: 100%;
  height: 330px;
  display: flex;
  justify-content: space-between;
  flex-direction: column;
  align-items: center;
  padding: 20px 0;
`

const Greeting = styled.div`
  color: white;
  background-color: black;
  font-size: 24px;
  width: 300px;
  padding: 10px 0;
  text-align: center;
`;

const InputsContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  align-items: center;
  flex: 1 0 auto;
`;

const Button = styled.div`
  width: 300px;
  height: 56px;
  border-radius: 2px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  border: 1px solid ${props => props.theme.mainGreen};
  background-color: ${props => props.alt ? props.theme.textSecondaryColor : props.theme.mainGreen};
  color: ${props => props.alt ? props.theme.mainGreen : props.theme.textSecondaryColor};
  flex: 0 0 auto;
  font-size: 24px;
`;

const UserMenuItem = styled.div`
  background-color: #e2e2e2;
  width: 100%;
  margin-bottom: 10px;
  padding: 2px;
  ${props => props.logout ? `color: grey` : ''};
  cursor: pointer;
  text-align: center;
`;

const Settings = styled.div`
  width: 90%;
  max-width: 370px;
  padding: 15px 0;
  display: flex;
  flex: 0 0 auto;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const Actions = styled.div`
  padding-top: 10px;
  margin: 10px 0;
  width: 90%;
  max-width: 370px;
  display: flex;
  flex: 0 0 auto;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-between;
  align-items: center;
`;

const ActionSquare = styled.div`
  background-color: black;
  border-radius: 2px;
  color: white;
  width: 48%;
  height: 80px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`

const SquareTitle = styled.div`
  font-size: 12px;
  position: relative;
  top: -15px;
  width: 90%;
`

const CreditNumber = styled.div`
  height: 30px;
  font-size: 30px;
  display: flex;
  align-items: center;
`

const WantCoffeeMessage = styled.div`
  background-color: black;
  color: white;
  margin-bottom: 10px;
  padding: 5px 10px;
  font-size: 16px;
`

export {
  ProfileScreen,
  Text,
  InputsContainer,
  Button,
  UserMenuItem,
  Settings,
  Actions,
  Greeting,
  Header,
  ActionSquare,
  SquareTitle,
  CreditNumber,
  WantCoffeeMessage
};
