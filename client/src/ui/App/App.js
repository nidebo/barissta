import styled from 'styled-components'

const AppContainer = styled.div`
  width: 100vw;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  align-items: center;
`

export { AppContainer }
