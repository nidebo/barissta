import styled from 'styled-components'

const SignupContainer = styled.div`
  width: 250px;
  display: flex;
  flex: 1 0 auto;
  border-radius: 2px;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
`;

const Quote = styled.div`
  font-family: Georgia, serif;
  font-style: italic;
  color: white;
  text-align: center;
  width: 200px;
  margin: 0 auto;
`

const QuoteAuthor = styled.div`
  font-family: Georgia, serif;
  font-style: italic;
  color: white;
  text-align: right;
  width: 250px;
`

export {
  SignupContainer,
  Quote,
  QuoteAuthor
}
