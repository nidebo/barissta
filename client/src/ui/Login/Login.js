import styled from 'styled-components'
import backgroundImage from '../../components/Login/img/marilyn.jpg'

const LoginScreen = styled.div`
  width: 100%;
  overflow: hidden;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  background: linear-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0)), url(${backgroundImage});
  background-position: center center;
  background-size: cover;
`;

const LoginContainer = styled.div`
  width: 250px;
  display: flex;
  flex: 1 0 auto;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
`;

const SignupBox = styled.div`
  background-color: rgba(255, 255, 255, 0.12);
  color: white;
  width: 100%;
  height: 50px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const ForgotPasswordLink = styled.div`
  margin-bottom: 35px;
  color: white;
  text-decoration: underline;
  font-size: 14px;
  text-align: center;
`;

const Text = styled.div`
  margin-top: 40px;
  margin-bottom: 20px;
  text-align: center;
  padding: 10px;
  width: 300px;
  color: white;
  background-color: black;
  font-size: 22px;
`;

const InputsContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  align-items: center;
  margin-bottom: 20px;
`;

const Input = styled.input`
  width: 100%;
  outline: none;
  margin-bottom: 10px;
  height: 40px;
  font-size: 14px;
  box-sizing: border-box;
  padding: 0 10px;
`;

const Button = styled.div`
  width: 100%;
  height: 40px;
  border: 1px solid white;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  color: white;
  flex: 0 0 auto;
  font-size: ${props => props.signup ? '16px;' : '20px;' };
  margin-bottom: 30px;
`;

const SeparatorBlock = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: 10px 0;
  color: white;
  width: 100%;
`

const SeparatorLine = styled.div`
  width: 110px;
  height: 1px;
  background-color: white;
`

const Loading = styled.div`
  position: fixed;
  display: flex;
  align-items: center;
  justify-content: center;
  top: 150px;
  margin-left: auto;
  margin-right: auto;
  left: 0;
  right: 0;
  background-color: rgba(234, 234, 234, 0.53);
  color: black;
  font-size: 38px;
  border-radius: 40px;
  height: 60px;
  width: 60px;
  box-shadow: 2px 3px 12px #4a4747;
`

const FlashMessage = styled.div`
  width: 100%;
  height: 30px;
  display: flex;
  position: absolute;
  top: 0;
  justify-content: center;
  align-items: center;
  margin-bottom: 10px;
  background-color: ${props => props.success ? '#07c566;' : '#ff4f6b;'}
  color: white;
  font-size: 0.9em;
`;

const RecoverPasswordBlock = styled.div`
  width: 250px;
  display: flex;
  flex: 1 0 auto;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
`

export { 
  LoginScreen,
  LoginContainer,
  SignupBox,
  ForgotPasswordLink,
  Text,
  Input,
  InputsContainer,
  Button,
  SeparatorLine,
  SeparatorBlock,
  Loading,
  FlashMessage,
  RecoverPasswordBlock
}
