import styled from 'styled-components'

const SidebarItem = styled.div`
  font-size: 16px;
  display: flex;
  width: 150px;
  color: ${props => props.theme.textMainColor};
  cursor: pointer;
  margin-top: 20px;
  padding-left: 25px;

  &:hover {
    color: ${props => props.theme.mainGreen};
  }

  &:active {
    color: ${props => props.theme.mainGreen};
  }

  &.active {
    text-transform: capitalize;
  }
`;

const Separator = styled.div`
  border-top: 1px solid #e6e6e6;
  margin-left: 20px;
  margin-top: 20px;
  width: 60%;
`;

const SidebarBlock = styled.div`
  z-index: 2 !important;
  position: fixed !important;
  top: 50px !important;
  background-color: white;
  color: black;
  width: 70% !important;
  max-width: 350px !important;
`

export { 
  SidebarItem,
  Separator,
  SidebarBlock
}
