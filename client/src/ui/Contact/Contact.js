// npm packages
import styled from 'styled-components'

const Text = styled.div`
  font-size: 20px;
  margin-top: 30px;
  
  @media screen and (min-width: 767px) {
    font-size: 24px;
    margin-top: 30px;
    margin-bottom: 40px;
  }
`;

const ContactDetails = styled.div`
  display: flex;
  padding-left: 40px;
  flex-direction: column;
  justify-content: space-around;
  align-items: flex-start;

  @media screen and (min-width: 767px) {
    flex-direction: row;
    align-items: center;
  }
`;

const ContactContainer = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-between;
  box-sizing: border-box;
  flex-direction: column;
  margin-bottom: 30px;
  flex-wrap: wrap;
`;

export { Text, ContactContainer, ContactDetails };