import styled from 'styled-components'

const MapSectionContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  box-sizing: border-box;
  flex-wrap: wrap;
  margin-bottom: 66px;
`;

const ColumnContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1 1 100%;
  align-items: center;

  @media screen and (max-width: 768px) {
    flex: 1 1 300px;
  }
`;

const MapContainer = styled.div`
  height: 284px;
  width: 100%;

  @media screen and (min-width: 768px) {
    height: 400px;
  }
`;

export {
  MapSectionContainer,
  ColumnContainer,
  MapContainer
}
