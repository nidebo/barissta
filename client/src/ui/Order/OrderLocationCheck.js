import styled from 'styled-components'

const LocationCheckScreen = styled.div`
  height: 100%;
  width: 100%;
  background-image: linear-gradient(135deg, #0E5CAD 10%, #79F1A4 100%);
  opacity: 0.98;
  position: fixed;
  z-index: 1;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`

const LocationCheckContent = styled.div`
  height: 80%;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
`

const LocationCheckTitle = styled.div`
  background-color: black;
  color: white;
  font-size: 24px;
  text-align: center;
  padding: 10px;
  width: 260px;
  margin: 0 auto;
`

const LocationCheckDescription = styled.div`
  color: white;
  font-weight: bold;
  font-size: 22px;
  width: 230px;
  text-align: center;
`

const LocationCheckButton = styled.div`
  color: white;
  font-weight: bold;
  border: 1px solid white;
  width: 280px;
  font-size: 22px;
  height: 50px;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: rgba(255, 251, 251, 0.16);
`

export {
  LocationCheckScreen,
  LocationCheckContent,
  LocationCheckTitle,
  LocationCheckDescription,
  LocationCheckButton
}
