import styled from 'styled-components';

const OrderSummaryContainer = styled.div`
  height: 40px;
  width: 94%;
  display: flex;
  flex: 0 0 auto;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  color: black;
  background-color: white;
  margin: 10px auto;
  font-size: 16px;
  font-family: 'Source Sans Pro', sans-serif;
  border: 2px solid black;
  font-weight: bold;
  padding: 0 14px;
`

const OrderSummaryCount = styled.div`
  display: flex;
  margin-left: 10px;
`

const OrderSummaryLine = styled.div`
  display: flex;
  align-items: center;
`

const TotalItemCount = styled.div`
  color: ${props => props.theme.mainGreen};
`

const TotalPrice = styled.div`
  &.discount {
    color: #a9a6a6;
  }
`

export {
  OrderSummaryContainer,
  OrderSummaryCount,
  OrderSummaryLine,
  TotalItemCount,
  TotalPrice
};
