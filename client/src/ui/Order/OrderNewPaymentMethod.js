import styled from 'styled-components'

const PaymentMethodForm = styled.form`
  padding: 20px;
  width: 250px;
  border-radius: 2px;
  border: 2px solid ${props => props.theme.mainGreen};
  font-weight: bold;
  font-family: 'Source Sans Pro', sans-serif;
  font-size: 13px;
`

const PaymentMethodField = styled.div`
  background: transparent;
  font-weight: 300;
  color: #31325F;
  outline: none;
  flex: 1;
  cursor: text;
`

const PaymentMethodBlock = styled.div`
  width: 100%;
  margin: 10px 0;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`

const CardErrors = styled.div`
  margin-top: 10px;
  color: red;
  font-family: 'Source Sans Pro', sans-serif;
`

export {
  PaymentMethodForm,
  PaymentMethodField,
  PaymentMethodBlock,
  CardErrors
}