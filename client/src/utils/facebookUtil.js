import { fbAppId } from './config'

const loadFacebookSDK = (callback) => {
  window.fbAsyncInit = () => {
    window.FB.init({
      appId      : fbAppId,
      cookie     : true,
      xfbml      : true,
      version    : 'v2.9'
    })
    if (callback) {
      callback()
    }
  }

  (function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
}

export default loadFacebookSDK