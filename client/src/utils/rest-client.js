import 'whatwg-fetch'

// utils
import { getTokenCookie } from './manageCookies'
import { apiHost } from './config';

const getHeaders = () => {
  const headers = new Headers({
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  });
  const token = getTokenCookie();
  if (token) {
    headers.append('authorization', `Bearer ${token}`);
  }
  return headers;
}

const getMultipartHeaders = () => {
  const headers = new Headers({
    'Accept': 'application/json'
  })
  const token = getTokenCookie()
  if (token) {
    headers.append('authorization', `Bearer ${token}`)
  }
  return headers
}

const get = (url) => {
  return fetch(apiHost.concat(url), {
    method: 'GET',
    headers: getHeaders(),
  });
}

const postMultipart = (url, data) => {
  const formData  = new FormData();
  for (let name in data) {
    formData.append(name, data[name]);
  }
  console.log(JSON.stringify(formData))
  return fetch(apiHost.concat(url), {
    method: 'POST',
    headers: getMultipartHeaders(),
    body: formData
  })
}

const post = (url, body) => {
  return fetch(apiHost.concat(url), {
    method: 'POST',
    headers: getHeaders(),
    body: JSON.stringify(body),
  });
}

const put = (url, body) => {
  return fetch(apiHost.concat(url), {
    method: 'PUT',
    headers: getHeaders(),
    body: JSON.stringify(body),
  });
}

const remove = (url, body) => {
  return fetch(apiHost.concat(url), {
    method: 'DELETE',
    headers: getHeaders(),
    body: JSON.stringify(body),
  });
}

const RestClient = {
  get,
  post,
  postMultipart,
  put,
  remove
}

export default RestClient;
