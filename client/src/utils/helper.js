const parseURLQuery = query => {
  const result = {}
  const parametersArray = query.substr(1).split('&')
  parametersArray.forEach(element => {
    const [param, value] = element.split('=')
    result[param] = value
  })
  return result
}

const getFormattedPrice = price => {
  if (typeof price !== 'number') return ''
  const roundedPrice = Math.round( price * 100 + Number.EPSILON ) / 100
  return roundedPrice.toLocaleString('es',
    { 
      style: 'currency',
      currency: 'EUR',
      currencyDisplay: 'symbol'
    }
  )
}

const getDisplayName = name => {
  if (!name) return ''
  const parsedName = name.trim().split(' ')[0]
  return parsedName.charAt(0).toUpperCase() + parsedName.slice(1)
}

export {
  parseURLQuery,
  getFormattedPrice,
  getDisplayName
}
