import cookie from 'react-cookie'
import jwtDecode from 'jwt-decode'
import moment from 'moment'

const getTokenCookie = () => {
  try {
    return cookie.load('barisstaToken');
  } catch (e) {
    console.error('Error getting a token cookie: ', e);
    return false;
  }
};

const setTokenCookie = token => {
  try {
    const decodedToken = jwtDecode(token)
    const expires = new Date(decodedToken.exp * 1000)
    cookie.save('barisstaToken', token, { path: '/', expires });
    return true;
  } catch (e) {
    console.error('Error setting a token cookie: ', e);
    return false;
  }
};

const deleteTokenCookie = () => {
  try {
    cookie.remove('barisstaToken', { path: '/' });
    return true;
  } catch (e) {
    console.error('Error deleting a token cookie: ', e);
    return false;
  }
};

const getCookieWarningCookie = () => {
  try {
    const cwc = cookie.load('cwc');
    return cwc;
  } catch (e) {
    console.error('Error getting cwc cookie: ', e);
    return false;
  }
};

const setCookieWarningCookie = () => {
  try {
    const expires = moment().add(10, 'year').toDate()
    cookie.save('cwc', 1, { path: '/', expires });
    return true;
  } catch (e) {
    console.error('Error setting cwc cookie: ', e);
    return false;
  }
};

const isUserAdmin = () => {
  try {
    const token = getTokenCookie()
    const decodedToken = jwtDecode(token)
    return decodedToken.isAdmin
  } catch (e) {
    console.log('Error checking admin: ', e)
    return false
  }
}

export { 
  getTokenCookie,
  setTokenCookie,
  deleteTokenCookie,
  isUserAdmin,
  getCookieWarningCookie,
  setCookieWarningCookie
}
