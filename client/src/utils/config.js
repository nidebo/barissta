const clientHost = process.env.REACT_APP_CLIENT_HOST
const apiHost = process.env.REACT_APP_API_HOST
const stripeKey = process.env.REACT_APP_STRIPE_KEY
const fbAppId = process.env.REACT_APP_FACEBOOK_APP_ID
const googleMapsAPIKey = process.env.REACT_APP_GOOGLE_MAPS_KEY

export {
  stripeKey,
  clientHost,
  apiHost,
  fbAppId,
  googleMapsAPIKey
}
