import React from 'react'
import { connect } from 'react-redux';

import JumbotronLogo from './../../components/JumbotronLogo/JumbotronLogo'
import BarisstaDescription from './../../components/Home/BarisstaDescription'
import HomeSeparator from './../../components/Home/HomeSeparator'
import LeadSection from './../../components/Home/LeadSection'
import ShopsView from './../../components/Home/ShopsView'
import SuggestShop from './../../components/Home/SuggestShop'
import WorkWithBarissta from './../../components/Home/WorkWithBarissta'
import Contact from './../../components/Contact/Contact'
import Footer from './../../components/Footer/Footer'

import { getAllShops } from './../../redux/modules/shop/get'
import { fetchCurrentLocation } from './../../redux/modules/location/actions'

class HomeContainer extends React.PureComponent {
  componentDidMount() {
    const { getAllShops, fetchCurrentLocation } = this.props;
    getAllShops();
    fetchCurrentLocation();
  }

  render() {
    const { shops, currentCity } = this.props;
    return (
      <div style={{ flex: '1 1 auto', width: '100%' }}>
        <JumbotronLogo />
        <BarisstaDescription />
        <HomeSeparator />
        <ShopsView shops={shops} currentCity={currentCity} />
        <HomeSeparator negativeMargin />
        <LeadSection />
        <SuggestShop />
        <WorkWithBarissta />
        <Contact />
        <Footer shops={shops}/>
      </div>
    );
  }
}

const mapStateToProps = ({ shopsGet, locationActions }) => {
  return {
    shops: shopsGet.Shops.data,
    currentCity: locationActions.location
  }
};

HomeContainer = connect(mapStateToProps, {
  getAllShops,
  fetchCurrentLocation
})(HomeContainer);

export default HomeContainer;
