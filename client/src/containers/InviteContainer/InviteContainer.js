import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';

import Invite from './../../components/Invite/Invite';
import { fetchPromoCode } from './../../redux/modules/invite/actions';

class InviteContainer extends React.PureComponent {
  componentDidMount() {
    this.props.dispatch(fetchPromoCode());
  }

  render() {
    const { promoCode, User } = this.props;
    if (Object.keys(User.data).length <= 0) {
      return <Redirect to="/login" />;
    }
    return (
      <Invite promoCode={promoCode} />
    );
  }
}

const mapStateToProps = ({ inviteActions, userSet }) => {
  return {
    promoCode: inviteActions.promoCode.data,
    User: userSet.User
  }
};

InviteContainer = connect(mapStateToProps)(InviteContainer);

export default InviteContainer;
