import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';

// components
import Admin from './../../components/Admin/Admin';

// actions
import { fetchUsers } from './../../redux/modules/admin/actions';
import { fetchSummary } from './../../redux/modules/admin/getSummary';
import { fetchShops } from './../../redux/modules/admin/getShops';

// utils
import { isUserAdmin } from './../../utils/manageCookies';

class AdminContainer extends React.PureComponent {
  componentDidMount() {
    this.props.dispatch(fetchSummary());
    this.props.dispatch(fetchUsers());
    this.props.dispatch(fetchShops());
  }

  render() {
    if (!isUserAdmin()) {
      return <Redirect to='/profile' />;
    }
    const { users, shops, summary } = this.props;
    return (<Admin users={users} shops={shops} summary={summary} />);
  }
}

const mapStateToProps = ({ adminActions, adminSummaryActions, adminShopsActions }) => {
  return {
    users: adminActions.users,
    summary: adminSummaryActions.summary.data,
    shops: adminShopsActions.shops
  }
};

export default connect(mapStateToProps)(AdminContainer);
