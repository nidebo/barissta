import React from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router'
import _ from 'lodash'

// components
import CoffeeShop from './../../components/CoffeeShop/CoffeeShop'

// actions
import { getShop } from './../../redux/modules/shop/getShop'
import { initCart } from './../../redux/modules/cart/cart'

class CoffeeShopContainer extends React.PureComponent {
  state = {
    isCartUpdated: false
  }

  checkCartUpdated = (shop, cart) => {
    const shopSlug = _.get(shop, 'data.slug')
    const cartShopSlug = cart.shop
    return !!shop && !!cart && shopSlug === cartShopSlug
  }

  componentDidMount() {
    const { getShop, match, Shop, cart } = this.props
    const slug = match.params.slug
    if (_.isEmpty(Shop) || Shop.data.slug !== slug) {
      getShop(match.params.slug)
    } else if (this.checkCartUpdated(Shop, cart)) {
      this.setState({ isCartUpdated: true })
    }
  }

  componentWillReceiveProps(nextProps) {
    if(this.checkCartUpdated(nextProps.Shop, nextProps.cart)) {
      this.setState({ isCartUpdated: true })
    }
  }

  render() {
    const { Shop, cart, user } = this.props
    const { isCartUpdated } = this.state

    if (!_.isEmpty(Shop.error)) {
      return <Redirect to='/' />
    }

    return (<CoffeeShop Shop={Shop.data} user={user} cart={cart} isCartUpdated={isCartUpdated} />)
  }
}

const mapStateToProps = ({ shopGet, cart, userSet }) => {
  return {
    Shop: shopGet.Shop,
    cart,
    user: userSet.User.data
  }
}

CoffeeShopContainer = connect(mapStateToProps, {
  getShop,
  initCart
})(CoffeeShopContainer)

export default CoffeeShopContainer
