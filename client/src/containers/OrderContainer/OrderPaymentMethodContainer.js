import React from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import _ from 'lodash'

import OrderPaymentMethod from './../../components/Order/OrderPaymentMethod'
import OrderNewPaymentMethod from './../../components/Order/OrderNewPaymentMethod'

// actions
import { getShop } from './../../redux/modules/shop/getShop'
import { addPaymentMethod } from './../../redux/modules/user/addPaymentMethod'
import { setSigningForPurchase, unsetSigningForPurchase } from './../../redux/modules/user/signForPurchase'

// utils
import { getTokenCookie } from './../../utils/manageCookies'

class OrderPaymentMethodContainer extends React.PureComponent {
  componentDidMount() {
    const { shop, getShop, match } = this.props
    if (_.isEmpty(shop)) {
      getShop(match.params.slug)
    }
  }

  componentWillMount() {
    const { shop, match, setSigningForPurchase, unsetSigningForPurchase } = this.props
    if (!getTokenCookie()) {
      return setSigningForPurchase(match.params.slug)
    } else if (shop.items) {
      unsetSigningForPurchase()
    }
  }

  render() {
    const { match, cart, user, shop, addPaymentMethod,
            addPaymentMethodState } = this.props
    if (!getTokenCookie()) {
      return <Redirect to='/login' />
    }
    if (!shop.items) {
      return <Redirect to={`/coffee-shop/${match.params.slug}`} />
    }
    if (_.isEmpty(user.card)) {
      return <OrderNewPaymentMethod 
              user={user}
              cart={cart}
              onAddPaymentMethod={addPaymentMethod}
              addPaymentMethodState={addPaymentMethodState} />
    }
    return (
      <OrderPaymentMethod user={user} cart={cart} shop={shop} />
    )
  }
}

const mapStateToProps = ({ shopGet, cart, userSet, userAddPaymentMethod }) => {
  return {
    shop: shopGet.Shop.data,
    cart,
    user: userSet.User.data,
    addPaymentMethodState: userAddPaymentMethod
  };
};

OrderPaymentMethodContainer = connect(mapStateToProps, {
  getShop,
  addPaymentMethod,
  setSigningForPurchase,
  unsetSigningForPurchase
})(OrderPaymentMethodContainer)

export default OrderPaymentMethodContainer
