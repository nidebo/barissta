import React from 'react'
import { connect } from 'react-redux'
import _ from 'lodash'

import OrderSelectShop from './../../components/Order/OrderSelectShop'

import { getAllShops } from './../../redux/modules/shop/get'
import { fetchCurrentLocation } from './../../redux/modules/location/actions'

class OrderSelectShopContainer extends React.PureComponent {
  componentDidMount() {
    const { shops, currentCity, getAllShops, fetchCurrentLocation } = this.props;
    if (_.isEmpty(shops)) {
      getAllShops();
    }
    if (_.isEmpty(currentCity.data)) {
      fetchCurrentLocation();
    }
  }

  render() {
    const { shops, currentCity } = this.props;
    return (
      <OrderSelectShop shops={shops} currentCity={currentCity} />
    )
  }
}

const mapStateToProps = ({ shopsGet, locationActions }) => {
  return {
    shops: shopsGet.Shops.data,
    currentCity: locationActions.location
  }
};

OrderSelectShopContainer = connect(mapStateToProps, {
  getAllShops,
  fetchCurrentLocation
})(OrderSelectShopContainer)

export default OrderSelectShopContainer
