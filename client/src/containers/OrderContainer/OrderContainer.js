import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import _ from 'lodash'

import Order from './../../components/Order/Order'

class OrderContainer extends React.PureComponent {
  static propTypes = {
    scrollLocked: PropTypes.bool,
    isCheckoutView: PropTypes.bool.isRequired
  }

  getItemsToDisplay = () => {
    const { isCheckoutView, cart, shop } = this.props
    if (isCheckoutView) {
      const items = Object.values(cart.items)
      return items;
    }
    return shop.items || [];
  }

  render() {
    const { cart, scrollLocked, isCheckoutView } = this.props
    if (_.isUndefined(cart)) return null
    return (
      <Order isCheckoutView={isCheckoutView} scrollLocked={scrollLocked} cart={cart} items={this.getItemsToDisplay()} />
    )
  }
}

const mapStateToProps = ({ shopGet, cart }) => {
  return {
    shop: shopGet.Shop.data,
    cart
  }
}

OrderContainer = connect(mapStateToProps, null)(OrderContainer)

export default OrderContainer
