import React from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router'

// components
import ChangePassword from './../../components/ChangePassword/ChangePassword'

// actions
import { changePassword } from './../../redux/modules/user/actions'

class ChangePasswordContainer extends React.PureComponent {
  render() {
    const {User, changePassword} = this.props;
    if (Object.keys(User.data).length <= 0) {
      return <Redirect to='/login' />;
    }
    return (
      <ChangePassword User={User} changePassword={changePassword} />
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    User: state.userSet.User,
  };
};

ChangePasswordContainer = connect(mapStateToProps, {
  changePassword,
})(ChangePasswordContainer);

export default ChangePasswordContainer;
