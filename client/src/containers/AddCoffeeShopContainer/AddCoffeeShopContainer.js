import React from 'react';
import { Redirect } from 'react-router';
import { connect } from 'react-redux';

// components
import AddCoffeeShop from './../../components/AddCoffeeShop/AddCoffeeShop'

// actions
import { addShop } from './../../redux/modules/shop/set';

import { isUserAdmin } from './../../utils/manageCookies';

class AddCoffeeShopContainer extends React.PureComponent {
  render() {
    const { addShop, newShopState } = this.props;
    if (!isUserAdmin()) {
      return <Redirect to='/profile' />;
    }
    return (
      <AddCoffeeShop addCoffeeShop={addShop} newShopState={newShopState} />
    );
  }
}

const mapStateToProps = ({ shopSet }, ownProps) => {
  return {
    newShopState: shopSet.Shop,
  };
};

AddCoffeeShopContainer = connect(mapStateToProps, {
  addShop,
})(AddCoffeeShopContainer);

export default AddCoffeeShopContainer;