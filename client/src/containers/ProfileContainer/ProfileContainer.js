import React from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router'

// components
import Profile from './../../components/Profile/Profile'

// actions
import { logoutUser } from './../../redux/modules/auth/logout'
import { hideTutorialAction } from './../../redux/modules/tutorial/actions'

// utils
import { getTokenCookie } from './../../utils/manageCookies'

class ProfileContainer extends React.PureComponent {

  render() {
    const { User, shouldShowTutorial, logoutUser, hideTutorialAction, isSigningForPurchase, currentShop } = this.props

    if (!getTokenCookie()) {
      return <Redirect to='/login' />
    }

    if (isSigningForPurchase) {
      const redirectUrl = `/order/payment?shop=${currentShop}`
      return <Redirect to={redirectUrl} />
    }

    return (
      <Profile
        User={User.data}
        shouldShowTutorial={shouldShowTutorial}
        logoutUser={logoutUser}
        hideTutorialAction={hideTutorialAction}
      />
    )
  }
}

const mapStateToProps = ({ userSet, tutorialActions, signForPurchaseActions }) => {
  return {
    User: userSet.User,
    shouldShowTutorial: tutorialActions.Tutorial.shouldShow,
    isSigningForPurchase: signForPurchaseActions.isSigningForPurchase,
    currentShop: signForPurchaseActions.shop
  }
}

ProfileContainer = connect(mapStateToProps, {
  logoutUser,
  hideTutorialAction,
})(ProfileContainer)

export default ProfileContainer
