import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

import { ShopsListContainer, Shop, ShopDescription,
         ShopName, List, ShopAddress, ShopAddressText } from './../../ui/ShopsList/ShopsList'

class ShopsList extends React.PureComponent {

  static propTypes = {
    shops: PropTypes.array.isRequired
  }

  renderShop = (shop, i) => {
    const shopImageUrl = `https://res.cloudinary.com/hp7atnn8j/${shop.image_url}`;
    const url = `/coffee-shop/${shop.slug}`
    return (
      <Link key={i} to={url}>
        <Shop key={i} imageUrl={shopImageUrl}>
          <ShopDescription>
            <ShopName>{shop.name}</ShopName>
            <ShopAddress>
              <i style={{fontSize: '12px', marginTop: '2px'}} className="fa fa-location-arrow" aria-hidden="true"></i>
              <ShopAddressText>{shop.address}</ShopAddressText>
            </ShopAddress>
          </ShopDescription>
        </Shop>
      </Link>
    )
  }

  render() {
    const { shops } = this.props
    return (
      <ShopsListContainer>
        <List>
          { shops.map((shop, i) => this.renderShop(shop, i))}
        </List>
      </ShopsListContainer>
    )
  }
}

export default ShopsList
