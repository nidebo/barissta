import React from 'react'
import PropTypes from 'prop-types'
import { HashLink as Link } from 'react-router-hash-link';
import MediaQuery from 'react-responsive'

import { HeaderContainer, HeaderLogo, HeaderItem, RightContent, MenuIconBlock } from './../../ui/Header/Header'

import Logo from './img/barissta-logo.png'
import MenuIcon from './img/ic_menu_black_24px.svg'

class Header extends React.PureComponent {
  static propTypes = {
    User: PropTypes.object,
    toggleSidebar: PropTypes.func.isRequired,
    hideSidebar: PropTypes.func.isRequired
  }

  render() {
    const { toggleSidebar, hideSidebar, User } = this.props
    const activeUser = User && Object.keys(User).length > 0

    return (
      <HeaderContainer>
        <Link to='/'><HeaderLogo maxHeight src={Logo} onClick={hideSidebar} /></Link>
        <MediaQuery query='(min-width: 768px)'>
          <RightContent>
            <HeaderItem><Link to="/#coffeeShops">Cafeterías</Link></HeaderItem>
            <HeaderItem><Link to="/#howItWorks">Cómo Funciona</Link></HeaderItem>
            <HeaderItem><Link to="/#contact">Contacto</Link></HeaderItem>
            <Link to='/login'>
              <HeaderItem className={(activeUser ? 'active' : '')}>
                <i className="fa fa-user" style={{"marginRight":"7px"}} aria-hidden="true"></i>
                {activeUser && User.name ? User.name.split(' ')[0] : 'Entrar'}
              </HeaderItem>
            </Link>
          </RightContent>
      </MediaQuery>
      <MediaQuery query='(max-width: 767px)'>
        <RightContent>
          <div onClick={toggleSidebar}><MenuIconBlock src={MenuIcon} /></div>
        </RightContent>
      </MediaQuery>
      </HeaderContainer>
    )
  }
}

export default Header
