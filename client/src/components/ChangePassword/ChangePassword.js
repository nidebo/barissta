import React from 'react'
import PropTypes from 'prop-types'

// components
import { LoginScreen, LoginContainer, Text, Input, InputsContainer, Button } from './../../ui/Login/Login'

class Login extends React.PureComponent {
  static propTypes = {
    changePassword: PropTypes.func.isRequired,
  }

  state = {
    oldPassword: '',
    password: '',
  }

  onInputKeyUp = (event) => {
    if (event.key === 'Enter') {
      this.onSubmitLogin();
    }
  }

  onOldPasswordChange = (event) => {
    this.setState({ oldPassword: event.target.value });
  }

  onPasswordChange = (event) => {
    this.setState({ password: event.target.value });
  }

  onSubmitLogin = () => {
    const {oldPassword, password} = this.state;
    const {changePassword} = this.props;
    changePassword({ oldPassword, password });
    this.setState({ oldPassword: '', password: '' });
  }

  render() {
    const {oldPassword, password} = this.state;
    return (
      <LoginScreen>
        <LoginContainer>
          <Text>Cambia tu contraseña</Text>
          <InputsContainer>
            <Input type='password' placeholder='contraseña actual' value={oldPassword} onChange={this.onOldPasswordChange} onKeyUp={this.onInputKeyUp} />
            <Input type='password' placeholder='contraseña nueva' value={password} onChange={this.onPasswordChange} onKeyUp={this.onInputKeyUp} />
          </InputsContainer>
          <Button onClick={this.onSubmitLogin}>Continuar</Button>
        </LoginContainer>
      </LoginScreen>
    );
  }
}

export default Login;
