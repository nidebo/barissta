import React from 'react'

import { ButtonContainer, Logo } from './../../ui/Util/FacebookButton'

export default function FacebookButton({ onClick }) {
  return (
    <ButtonContainer onClick={ onClick }>
      <Logo className="fa fa-facebook-square" aria-hidden="true" />
      <div>Continuar con Facebook</div>
    </ButtonContainer>
  )
}
