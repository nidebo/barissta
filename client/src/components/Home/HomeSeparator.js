import React from 'react'

import { Separator, CupIcon, SeparatorBlock } from './../../ui/Home/Home'

import Taza from './img/barissta_taza.png'

export default function HomeSeparator({ negativeMargin }) {
  const getClassName = () => {
    return negativeMargin ? 'negativeMargin' : ''
  }
  return (
    <SeparatorBlock className={getClassName()}>
      <Separator /><CupIcon src={Taza} alt='' /><Separator />
    </SeparatorBlock>
  )
}