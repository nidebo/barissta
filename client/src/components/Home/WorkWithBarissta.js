import React from 'react'

import { HomeBlockContainer, Highlighted } from './../../ui/Home/Home'
import { Title, WorkWithBarisstaContainer, WorkWithBarisstaButton } from './../../ui/Home/WorkWithBarissta'
import { LeadButtons } from './../../ui/Home/LeadSection'

export default function WorkWithBarissta() {
  return (
    <HomeBlockContainer>
      <WorkWithBarisstaContainer>
        <Title>¿Tienes una cafetería <Highlighted>Top</Highlighted>?</Title>
        <LeadButtons>
          <a href='https://barissta.typeform.com/to/GEPI7f' target='_blank'>
            <WorkWithBarisstaButton>Trabaja con Barissta</WorkWithBarisstaButton>
          </a>
        </LeadButtons>
      </WorkWithBarisstaContainer>
    </HomeBlockContainer>
  )
}
