import React from 'react'

import { HomeBlockContainer } from './../../ui/Home/Home'
import { SuggestShopContainer, Title, Description, SuggestShopButton } from './../../ui/Home/SuggestShop'

export default function SuggestShop() {
  return (
    <HomeBlockContainer>
      <SuggestShopContainer>
        <Title>
          <div>¿No encuentras tu cafetería favorita?</div>
          <div>¿O simplemente no estamos en tu ciudad?</div>
        </Title>
        <Description>Queremos descubrir las mejores cafeterías y nos encantan las recomendaciones</Description>
        <a href='https://barissta.typeform.com/to/UHHZKX' target='_blank'>
          <SuggestShopButton>¿Alguna sugerencia?</SuggestShopButton>
        </a>
      </SuggestShopContainer>
    </HomeBlockContainer>
  )
}
