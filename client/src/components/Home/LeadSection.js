import React from 'react'
import { Link } from 'react-router-dom'

import { HomeBlockContainer, Highlighted } from './../../ui/Home/Home'
import { ButtonsContainer, Button } from './../../ui/JumbotronLogo/JumbotronLogo'
import { Title, LeadContainer } from './../../ui/Home/LeadSection'

export default function LeadSection() {
  return (
    <HomeBlockContainer>
      <LeadContainer>
        <Title>"No hay nada como un buen <Highlighted>café</Highlighted>"</Title>
        <ButtonsContainer>
          <Link to="/order/select-shop"><Button>PEDIR</Button></Link>
        </ButtonsContainer>
      </LeadContainer>
    </HomeBlockContainer>
  )
}
