import React from 'react'
import PropTypes from 'prop-types'
import _ from 'lodash'

import { ColumnContainer, City, CitiesContainer,
         ViewSelector, View } from './../../ui/Home/ShopsView'

import MapSection from './../../components/MapSection/MapSection'
import ShopsList from './../../components/ShopsList/ShopsList'

class ShopsView extends React.PureComponent {
  constructor() {
    super()
    this.state = {
      openTooltip: {},
      city: 'madrid',
      view: 'list'
    }
  }

  static propTypes = {
    shops: PropTypes.array.isRequired,
    currentCity: PropTypes.object
  }

  componentWillReceiveProps(nextProps) {
    const { currentCity: nextCurrentCity } = nextProps
    const { city } = this.state
    const nextCity = _.get(nextCurrentCity, 'data.city', null)
    if (nextCity && nextCity !== city) {
      this.setState({ city: nextCity })
    }
  }

  onSelectCity = (city) => {
    this.setState({ city })
  }

  onSelectView = (view) => {
    this.setState({ view })
  }

  filterShopsByCity = (city, shops) => {
    return shops.filter((shop) => shop.city.toLowerCase() === city)
  }

  getCityClass = (city) => {
    return city === this.state.city ? 'selected' : ''
  }

  getViewClass = (view) => {
    return view === this.state.view ? 'selected' : ''
  }

  render() {
    const { city, view } = this.state
    const { shops } = this.props
    const cityShops = this.filterShopsByCity(city, shops)
    return (
      <ColumnContainer id='coffeeShops'>
        <CitiesContainer>
          <City className={this.getCityClass('madrid')} onClick={() => this.onSelectCity('madrid')}>MAD</City>
          <div style={{fontSize: '25px'}}>&bull;</div>
          <City className={this.getCityClass('valencia')} onClick={() => this.onSelectCity('valencia')}>VLC</City>
        </CitiesContainer>
        <ViewSelector>
          <View className={this.getViewClass('list')} onClick={() => this.onSelectView('list')}>
            <i className="fa fa-list-ul" aria-hidden="true" />
          </View>
          <View className={this.getViewClass('map')} onClick={() => this.onSelectView('map')}>
            <i className="fa fa-map-marker" aria-hidden="true" />
          </View>
        </ViewSelector>
        { view === 'map' && <MapSection city={city} shops={cityShops} />}
        { view === 'list' && <ShopsList shops={cityShops} /> }
      </ColumnContainer>
    );
  }
}

export default ShopsView
