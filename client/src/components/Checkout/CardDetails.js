import React from 'react'
import PropTypes from 'prop-types'

// components
import { CardImage, CardExpiration, CardNumber,
         Card, CardDots, CardLast4,
         CardInfo } from './../../ui/Checkout/CardDetails'

// images
import Visa from './img/Visa.png'
import MasterCard from './img/MasterCard.png'

class CardDetails extends React.PureComponent {
  constructor() {
    super()
    this.cardIcons = {
      Visa,
      MasterCard
    }
  }

  static propTypes = {
    paymentMethod: PropTypes.object.isRequired
  }

  render() {
    const { paymentMethod } = this.props
    return (
      <Card>
        <CardNumber>
          <CardDots>····</CardDots>
          <CardDots>····</CardDots>
          <CardDots>····</CardDots>
          <CardLast4>{paymentMethod.last4}</CardLast4>
        </CardNumber>
        <CardInfo style={{width: '100%'}}>
          <CardExpiration>
            {paymentMethod.exp_month}/{paymentMethod.exp_year}
          </CardExpiration>
          <CardImage src={this.cardIcons[paymentMethod.brand]} />
        </CardInfo>
      </Card>
    );
  }
}

export default CardDetails
