import React from 'react'
import PropTypes from 'prop-types'

// components
import { TutorialScreen, TutorialWelcome, Button, TutorialDescription,
         TutorialContent, TutorialStep } from './../../ui/Tutorial/Tutorial'
import { getDisplayName } from './../../utils/helper'

class Tutorial extends React.PureComponent {

  static propTypes = {
    userName: PropTypes.string.isRequired,
    onClickOk: PropTypes.func.isRequired
  }

  render() {
    const { userName, onClickOk } = this.props
    const displayName = getDisplayName(userName)
    return (
      <TutorialScreen>
        <TutorialContent>
          <TutorialWelcome>¡{displayName}, ya eres Barissta!</TutorialWelcome>
          <TutorialDescription>Desde tu panel de usuario podrás:</TutorialDescription>
          <div>
            <TutorialStep>–&nbsp;&nbsp;Realizar tus pedidos</TutorialStep>
            <TutorialStep>–&nbsp;&nbsp;Consultar tu saldo disponible</TutorialStep>
            <TutorialStep>–&nbsp;&nbsp;Ver el saldo acumulado para el próximo mes</TutorialStep>
          </div>
          <Button onClick={onClickOk}>¡ENTENDIDO!</Button>
        </TutorialContent>
      </TutorialScreen>
    );
  }
}

export default Tutorial
