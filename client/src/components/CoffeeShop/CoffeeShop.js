import React from 'react'
import { withRouter } from 'react-router'
import PropTypes from 'prop-types'
import _ from 'lodash'

import { CoffeeShopScreen, Header, ShopName,
         ShopLocation, CoffeeShopInfo } from './../../ui/CoffeeShop/CoffeeShop'
import { OrderDetails } from './../../ui/Order/Order'
import OrderContainer from './../../containers/OrderContainer/OrderContainer'
import OrderSummary from './../../components/Order/OrderSummary'
import OrderButton from './../../components/Order/OrderButton'

class CoffeeShop extends React.PureComponent {
  
  static propTypes = {
    Shop: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    cart: PropTypes.object.isRequired,
    isCartUpdated: PropTypes.bool.isRequired
  }

  state = {
    scrollLocked: false
  }

  componentDidMount() {
    const screen = document.getElementById('coffeeShopScreen')
    const events = ['scroll', 'touchmove', 'wheel']
    events.forEach(event => {
      screen.addEventListener(event, _.debounce(() => {
        if (window.pageYOffset >= 120) {
          return this.setState({ scrollLocked: true })
        }
        this.setState({ scrollLocked: false })
      }, 10))
    })
  }

  getScrollLockedClass = () => {
    const { scrollLocked } = this.state
    return scrollLocked ? 'locked' : ''
  }

  isOrderButtonDisabled = () => {
    const { cart } = this.props
    return _.isEmpty(cart.items)
  }

  getDiscountedPrice = () => {
    const { cart, user } = this.props
    const totalPrice = Object.values(cart.items).reduce((acc, item) => {
      const itemTotal = item.price * item.amount
      return Math.round((acc + itemTotal) * 100) / 100
    }, 0)
    return Math.max(0, totalPrice - user.currentCredit)
  }

  onClickCheckoutButton = () => {
    const { Shop, history } = this.props
    const discountedPrice = this.getDiscountedPrice()
    if (discountedPrice > 0 && discountedPrice < 0.5) {
      alert("El precio mínimo del pedido debe ser 0,50€")
    } else {
      const nextUrl = `/order/payment?shop=${Shop.slug}`
      return history.push(nextUrl)
    }
  }

  getTodayOpenHours = () => {
    const { Shop } = this.props
    if (!_.isEmpty(Shop)) {
      const weekDay = new Date().getUTCDay()
      return Shop.openHours[weekDay]
    }
  }

  render() {
    const { Shop, cart, isCartUpdated } = this.props
    const { scrollLocked } = this.state
    let googleMapsUrl;
    let shopImageUrl;
    if (!_.isEmpty(Shop)) {
      const { lat, lng } = Shop.coords
      googleMapsUrl = `https://maps.google.com/?q=${lat},${lng}`;
      shopImageUrl = `https://res.cloudinary.com/hp7atnn8j/${Shop.image_url}`;
    } else {
      googleMapsUrl = '#';
      shopImageUrl = '';
    }

    let addressDisplay
    if (Shop.address) {
      addressDisplay = Shop.address.split(',')[0]
    }
    return (
      <CoffeeShopScreen id='coffeeShopScreen'>
        <Header id='coffeeShopHeader' className={this.getScrollLockedClass()} imageUrl={shopImageUrl} >
          <ShopName className={this.getScrollLockedClass()}>{Shop.name}</ShopName>
          <CoffeeShopInfo className={this.getScrollLockedClass()}>
            <a href={googleMapsUrl}>
              <i className="fa fa-location-arrow" aria-hidden="true"></i>
              <ShopLocation>{addressDisplay}</ShopLocation>
            </a>
            <div>
              <i className="fa fa-clock-o" aria-hidden="true"></i>
              <span>Hoy: {this.getTodayOpenHours()}</span>
            </div>
          </CoffeeShopInfo>
        </Header>
        <OrderContainer scrollLocked={scrollLocked} isCheckoutView={false} />
        <OrderDetails>
          { isCartUpdated && <OrderSummary cart={cart} />}
          <OrderButton 
            onClickButton={this.onClickCheckoutButton}
            content={"Continuar"}
            isDisabled={this.isOrderButtonDisabled()}
          />
        </OrderDetails>
      </CoffeeShopScreen>
    )
  }
}

export default withRouter(CoffeeShop)
