import React from 'react'
import { withRouter } from 'react-router'
import PropTypes from 'prop-types'
import { HashLink as Link } from 'react-router-hash-link'
import { Sidebar, Menu } from 'semantic-ui-react'
import MediaQuery from 'react-responsive'

import { SidebarItem, Separator, SidebarBlock } from './../../ui/Sidebar/Sidebar'

class SidebarComponent extends React.PureComponent {
  static propTypes = {
    User: PropTypes.object,
    sidebarOpen: PropTypes.bool.isRequired,
    hideSidebar: PropTypes.func.isRequired
  }

  renderSidebarElements = () => {
    const { hideSidebar } = this.props
    const elements = [
      { text: 'Cafeterías', classN: 'fa-coffee', link: '/#coffeeShops'},
      { text: 'Contacto', classN: 'fa-phone', link: '/#contact'}
    ]
    return elements.map((element, k) => {
      const { classN, text, link } = element
      return (
        <Link to={link} key={k} style={{width: '150px', display: 'block'}}>
          <Menu.Item as={SidebarItem} sidebar onClick={hideSidebar}>
            <i className={"fa " + classN } style={{"marginRight":"7px"}} aria-hidden="true"></i>
            <div>{text}</div>
          </Menu.Item>
        </Link>
      )
    })
  }

  renderSidebarUser = () => {
    const { User, hideSidebar } = this.props
    const activeUser = User && Object.keys(User).length > 0
    return (
      <div>
        <Menu.Item><Separator></Separator></Menu.Item>
        <Link to='/login'>
          <Menu.Item as={SidebarItem} sidebar className={(activeUser ? 'active' : '')} onClick={hideSidebar}>
            <i className="fa fa-user" style={{"marginRight":"7px"}} aria-hidden="true"></i>
            {activeUser && User.name ? User.name.split(' ')[0] : 'Entrar'}
          </Menu.Item>
        </Link>
      </div>
    )
  }

  render() {
    const { sidebarOpen } = this.props
    return (
      <div>
        <MediaQuery query='(max-width: 767px)'>
          <Sidebar as={SidebarBlock} animation="overlay" visible={sidebarOpen} icon="labeled" vertical inline inverted>
          { this.renderSidebarElements() }
          { this.renderSidebarUser() }
          </Sidebar>
        </MediaQuery>
      </div>
    )
  }
}

export default withRouter(SidebarComponent)
