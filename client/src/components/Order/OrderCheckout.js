import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import _ from 'lodash'

import { OrderScreen, OrderDetails } from './../../ui/Order/Order'
import { BackButton, EmptyCart, CheckoutTitle, CheckoutTitles } from './../../ui/Order/OrderCheckout'
import OrderLocationCheck from './../../components/Order/OrderLocationCheck'
import OrderButton from './../../components/Order/OrderButton'
import OrderContainer from './../../containers/OrderContainer/OrderContainer'
import OrderSummary from './../../components/Order/OrderSummary'
import { Loading } from './../../ui/Login/Login'
import { Highlighted } from './../../ui/Home/Home'
import { getDisplayName } from './../../utils/helper'

class OrderCheckout extends React.PureComponent {
  static propTypes = {
    cart: PropTypes.object.isRequired,
    shop: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    removeItem: PropTypes.func.isRequired,
    doPay: PropTypes.func.isRequired,
    paymentState: PropTypes.string.isRequired
  }

  state = {
    isLocationChecked: false,
  }

  onClickCheckoutButton = () => {
    const { doPay, cart } = this.props
    doPay({ cart })
  }

  onClickRemoveItem = (item) => {
    this.props.removeItem(item.id)
  }

  renderOrderItems = () => {
    const { cart } = this.props
    if (_.isEmpty(cart.items)) {
      return (<EmptyCart>Pídete algo! Tu cesta está vacía.</EmptyCart>)
    }
    return (<OrderContainer isCheckoutView={true} />)
  }

  isOrderButtonDisabled = () => {
    const { cart, paymentState } = this.props
    if (paymentState === 'PENDING' || _.isEmpty(cart.items)) {
      return true
    }
    return false
  }

  renderLoading = () => {
    const { paymentState } = this.props
    if (paymentState === 'PENDING') {
      return (
        <Loading><i className="fa fa-spinner fa-spin" aria-hidden="true" /></Loading>
      )
    }
  }

  renderLocationCheckScreen = () => {
    const { isLocationChecked } = this.state
    const { shop, user } = this.props
    if (!isLocationChecked) {
      return (<OrderLocationCheck userName={user.name} shopName={shop.name} onClickOk={this.onClickLocationCheck} />)
    }
  }

  onClickLocationCheck = () => {
    this.setState({ isLocationChecked: true })
  }

  render() {
    const { shop, cart, user } = this.props
    const displayName = getDisplayName(user.name)
    return (
      <OrderScreen>
        {this.renderLocationCheckScreen()}
        {this.renderLoading()}
        <CheckoutTitles>
          <CheckoutTitle>¡Hola {shop.name}!</CheckoutTitle>
          <CheckoutTitle>Soy <Highlighted>{displayName}</Highlighted> y he pedido:</CheckoutTitle>
        </CheckoutTitles>
        {this.renderOrderItems()}
        
        <Link to={`/coffee-shop/${shop.slug}`}>
          <BackButton>Añadir más productos</BackButton>
        </Link>
        
        <OrderDetails>
          <OrderSummary cart={cart} />
          <OrderButton 
            onClickButton={this.onClickCheckoutButton}
            isDisabled={this.isOrderButtonDisabled()}
            content={"Confirmar pedido"}
          />
        </OrderDetails>
      </OrderScreen>
    )
  }
}

export default OrderCheckout
