import React from 'react'
import PropTypes from 'prop-types'
import _ from 'lodash'

import ItemContainer from './../../containers/ItemContainer/ItemContainer'

import OrderSummary from './../../components/Order/OrderSummary'

import { ItemsContainer, OrderBlock } from './../../ui/Order/Order'

class Order extends React.PureComponent {
  static propTypes = {
    cart: PropTypes.object.isRequired,
    items: PropTypes.array.isRequired,
    scrollLocked: PropTypes.bool,
    isCheckoutView: PropTypes.bool.isRequired
  }

  getOrderBlockClass = () => {
    const { scrollLocked, isCheckoutView } = this.props
    let className = ''
    if (scrollLocked) {
      className += 'locked '
    }
    if (isCheckoutView) {
      className += 'checkout '
    }
    return className
  }

  render() {
    const { items, cart } = this.props
    return (
      <OrderBlock className={this.getOrderBlockClass()}>
        <ItemsContainer>
          { !_.isEmpty(items) && items.map((item, i) => (
            <ItemContainer key={i} item={item} />
          ))}
        </ItemsContainer>
      </OrderBlock>
    )
  }
}

export default Order
