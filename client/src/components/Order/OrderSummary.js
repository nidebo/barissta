import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import _ from 'lodash'

import { OrderSummaryContainer, OrderSummaryCount,
         OrderSummaryLine, TotalItemCount, TotalPrice } from './../../ui/Order/OrderSummary'

import { getFormattedPrice } from './../../utils/helper'

class OrderSummary extends React.PureComponent {
  static propTypes = {
    cart: PropTypes.object.isRequired
  }

  getTotalItems = () => {
    const { cart } = this.props
    const totalItems = Object.values(cart.items).reduce((acc, item) => {
      return acc + item.amount
    }, 0)
    return (<TotalItemCount>{totalItems}</TotalItemCount>)
  }

  getTotalPrice = () => {
    const { cart } = this.props
    const totalPrice = Object.values(cart.items).reduce((acc, item) => {
      const itemTotal = item.price * item.amount
      return Math.round((acc + itemTotal) * 100) / 100
    }, 0)
    return totalPrice
  }

  getDiscountedPrice = () => {
    const { user } = this.props
    const totalPrice = this.getTotalPrice()
    return Math.max(0, totalPrice - user.currentCredit)
  }

  wouldApplyCredit = () => {
    const { user, cart } = this.props
    if (!_.isEmpty(user) && Object.keys(cart.items).length > 0) {
      return user.currentCredit > 0
    }
    return false
  }

  getTotalPriceClass = () => {
    return this.wouldApplyCredit() ? 'discount' : ''
  }

  render() {
    return (
      <OrderSummaryContainer>
        <OrderSummaryLine>
          <i style={{fontSize: '14px'}} className="fa fa-shopping-basket" aria-hidden="true"></i>
          <OrderSummaryCount>{this.getTotalItems()}</OrderSummaryCount>
        </OrderSummaryLine>
        <OrderSummaryLine style={{fontSize: '0.9em'}}>
          <TotalPrice style={{display: 'flex'}} className={this.getTotalPriceClass()}>
            <div>Total:</div>
            <OrderSummaryCount>{getFormattedPrice(this.getTotalPrice())}</OrderSummaryCount>
          </TotalPrice>
          { this.wouldApplyCredit() && (
            <div style={{display: 'flex', marginLeft: '14px'}}>
              <div>Por ser tú ;)</div>
              <OrderSummaryCount>{getFormattedPrice(this.getDiscountedPrice())}</OrderSummaryCount>
            </div>
          )}
        </OrderSummaryLine>
      </OrderSummaryContainer>
    )
  }
}

const mapStateToProps = ({ userSet }) => {
  return {
    user: userSet.User.data
  }
}

OrderSummary = connect(mapStateToProps, null)(OrderSummary)

export default OrderSummary
