import React from 'react'
import PropTypes from 'prop-types'

import { OrderButtonStyle } from './../../ui/Order/Order'

class OrderButton extends React.PureComponent {
  static propTypes = {
    onClickButton: PropTypes.func.isRequired,
    isDisabled: PropTypes.bool.isRequired,
    content: PropTypes.string.isRequired
  }

  state = {
    active: false
  }

  onClickButton = () => {
    const { onClickButton, isDisabled } = this.props
    if (isDisabled) return
    this.setState({ active: true })
    setTimeout(() => {
      this.setState({ active: false })
      onClickButton()
    }, 80)
  }

  getButtonClass = () => {
    const { isDisabled } = this.props
    const { active } = this.state
    if (isDisabled) {
      return 'disabled'
    }
    return active ? 'active' : ''
  }

  render() {
    const { content } = this.props
    return (
      <OrderButtonStyle
        onClick={this.onClickButton}
        className={this.getButtonClass()}
      >
        {content}
      </OrderButtonStyle>
    )
  }
}

export default OrderButton
