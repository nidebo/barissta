import React from 'react'
import PropTypes from 'prop-types'

import ShopsView from './../../components/Home/ShopsView'
import { OrderScreen, OrderTitle } from './../../ui/Order/Order'

class OrderSelectShop extends React.PureComponent {
  static propTypes = {
    shops: PropTypes.array.isRequired,
    currentCity: PropTypes.object
  }

  render() {
    const { shops, currentCity } = this.props
    return (
      <OrderScreen fixed>
        <OrderTitle>Selecciona una cafetería</OrderTitle>
        <ShopsView shops={shops} currentCity={currentCity} />
      </OrderScreen>
    )
  }
}

export default OrderSelectShop
