import React from 'react'
import PropTypes from 'prop-types'
import _ from 'lodash'
import { withRouter } from 'react-router'

import OrderSummary from './../../components/Order/OrderSummary'
import OrderButton from './../../components/Order/OrderButton'
import CardDetails from './../../components/Checkout/CardDetails'
import { OrderScreen, OrderTitle } from './../../ui/Order/Order'

class OrderPaymentMethod extends React.PureComponent {
  static propTypes = {
    cart: PropTypes.object.isRequired,
    shop: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired
  }

  isOrderButtonDisabled = () => {
    const { cart } = this.props
    return _.isEmpty(cart.items)
  }

  onClickCheckoutButton = () => {
    const { shop, history } = this.props
    const checkoutUrl = `/order/checkout?shop=${shop.slug}`
    return history.push(checkoutUrl)
  }

  render() {
    const { cart, user } = this.props
    return (
      <OrderScreen>
        <div style={{display: 'flex', flexDirection: 'column', flex: '1 0 auto'}}>
          <OrderTitle>Datos de pago</OrderTitle>
          <CardDetails paymentMethod={user.card} />
        </div>

        <OrderSummary cart={cart} />
        <OrderButton
          onClickButton={this.onClickCheckoutButton}
          isDisabled={this.isOrderButtonDisabled()}
          content={"Confirmar método de pago"}
        />
      </OrderScreen>
    )
  }
}

export default withRouter(OrderPaymentMethod)
