import React from 'react'
import PropTypes from 'prop-types'
import _ from 'lodash'

import { LocationCheckScreen, LocationCheckTitle, LocationCheckDescription,
         LocationCheckButton, LocationCheckContent } from './../../ui/Order/OrderLocationCheck'
import { getDisplayName } from './../../utils/helper'

class OrderLocationCheck extends React.PureComponent {
  static propTypes = {
    shopName: PropTypes.string.isRequired,
    userName: PropTypes.string.isRequired,
    onClickOk: PropTypes.func.isRequired
  }

  render() {
    const { shopName, userName, onClickOk } = this.props
    const displayName = getDisplayName(userName)
    return (
        <LocationCheckScreen>
          <LocationCheckContent>
            <LocationCheckTitle>Turno de {shopName}</LocationCheckTitle>
            <LocationCheckDescription>¡IMPORTANTE!</LocationCheckDescription>
            <LocationCheckDescription>{displayName}, muestra la SIGUIENTE pantalla en la cafetería para que confirmen tu pedido :)</LocationCheckDescription>
            <LocationCheckButton onClick={onClickOk}>¡ENTENDIDO!</LocationCheckButton>
          </LocationCheckContent>
        </LocationCheckScreen>
    )
  }
}

export default OrderLocationCheck
