import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

import { 
  LoginScreen, SeparatorBlock, SeparatorLine,
  Text, SignupBox, Button, Loading
} from './../../ui/Login/Login'
import { Quote, QuoteAuthor, SignupContainer } from './../../ui/Signup/Signup'
import { Highlighted } from './../../ui/Home/Home'
import FacebookButton from './../../components/Util/FacebookButton'
import loadFacebookSDK from './../../utils/facebookUtil'

class Signup extends React.PureComponent {
  static propTypes = {
    onFacebookLogin: PropTypes.func.isRequired,
    facebookLoginState: PropTypes.object.isRequired
  }

  onClickFacebookLogin = () => {
    const { onFacebookLogin } = this.props
    if (!window.FB) return
    window.FB.login(response => {
      const { accessToken: token } = response.authResponse
      onFacebookLogin({ token })
    }, { scope: 'public_profile,email,user_friends' })
  }

  renderLoading = () => {
    const { facebookLoginState } = this.props
    if (facebookLoginState.isLogging) {
      return (
        <Loading><i className="fa fa-spinner fa-spin" aria-hidden="true" /></Loading>
      )
    }
  }

  componentDidMount() {
    if (window.FB) return
    loadFacebookSDK()
  }

  render() {
    return (
      <LoginScreen>
        <SignupContainer signup>
          { this.renderLoading() }
          <Text>REGÍSTRATE EN <Highlighted>BARISSTA</Highlighted></Text>
          <div style={{marginTop: '32px'}}>
            <Quote>“El café seguirá caliente cuando termines.”</Quote>
            <QuoteAuthor>M.M.</QuoteAuthor>
          </div>
          <div style={{width: '100%', marginTop: '50px'}}>
            <FacebookButton onClick={this.onClickFacebookLogin} />
            <SeparatorBlock>
              <SeparatorLine />
              <div>O</div>
              <SeparatorLine />
            </SeparatorBlock>
            <Link to='/signup/email' style={{width: '100%'}}>
              <Button signup>Regístrate con tu Correo Electrónico</Button>
            </Link>
          </div>
        </SignupContainer>
        <Link to='/login' style={{width: '100%'}}>
          <SignupBox>
            <div>¿Ya tienes cuenta? Inicia sesión</div>
          </SignupBox>
      </Link>
      </LoginScreen>
    )
  }
}

export default Signup
