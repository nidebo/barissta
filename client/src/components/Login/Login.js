import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

import {
  LoginScreen,
  LoginContainer,
  SignupBox,
  ForgotPasswordLink,
  Text,
  Input,
  InputsContainer,
  Button,
  SeparatorBlock,
  SeparatorLine,
  Loading,
  FlashMessage
} from './../../ui/Login/Login'
import { Highlighted } from './../../ui/Home/Home'
import FacebookButton from './../../components/Util/FacebookButton'
import loadFacebookSDK from './../../utils/facebookUtil'

class Login extends React.PureComponent {
  static propTypes = {
    onLogin: PropTypes.func.isRequired,
    loginState: PropTypes.object.isRequired
  }

  state = {
    email: '',
    password: '',
    errorMessage: ''
  }

  onInputKeyUp = event => {
    if (event.key === 'Enter') {
      this.onSubmitLogin()
    }
  }

  onEmailChange = event => {
    this.setState({ email: event.target.value })
  }

  onPasswordChange = event => {
    this.setState({ password: event.target.value })
  }

  onSubmitLogin = () => {
    const { email, password } = this.state
    const { onLogin } = this.props
    onLogin({ email, password })
  }

  componentWillReceiveProps(nextProps) {
    const { loginState } = nextProps
    this.setState({ errorMessage: loginState.errorMessage })
    if (loginState.errorMessage) {
      window.scrollTo(0, 0)
    }
  }

  renderLoading = () => {
    const { loginState } = this.props
    if (loginState.isLogging) {
      return (
        <Loading><i className="fa fa-spinner fa-spin" aria-hidden="true" /></Loading>
      )
    }
  }

  onClickFacebookLogin = () => {
    const { onLogin } = this.props
    if (!window.FB) return
    window.FB.login(response => {
      const { accessToken: token } = response.authResponse
      onLogin({ token })
    }, { scope: 'public_profile,email,user_friends' })
  }

  componentDidMount() {
    if (window.FB) return
    loadFacebookSDK()
  }

  render() {
    const { email, password, errorMessage } = this.state
    return (
      <LoginScreen>
        {errorMessage && <FlashMessage>{errorMessage}</FlashMessage>}
        <LoginContainer>
          { this.renderLoading() }
          <Text>INICIA SESIÓN EN <Highlighted>BARISSTA</Highlighted></Text>
          <div style={{width: '100%'}}>
            <FacebookButton onClick={this.onClickFacebookLogin} />
            <SeparatorBlock>
              <SeparatorLine />
              <div>O</div>
              <SeparatorLine />
            </SeparatorBlock>
            <InputsContainer>
              <Input
                type="email"
                placeholder="Correo electrónico"
                value={email}
                onChange={this.onEmailChange}
                onKeyUp={this.onInputKeyUp}
              />
              <Input
                type="password"
                placeholder="Contraseña"
                value={password}
                onChange={this.onPasswordChange}
                onKeyUp={this.onInputKeyUp}
              />
            </InputsContainer>
          </div>
          <div style={{width: '100%'}}>
            <ForgotPasswordLink><Link to="/recover-password">¿Has olvidado tu contraseña?</Link></ForgotPasswordLink>
            <Button onClick={this.onSubmitLogin}>Entrar</Button>
          </div>
        </LoginContainer>
        <Link to="/signup" style={{width: '100%'}}>
          <SignupBox>
            <div>¿No tienes cuenta? Regístrate</div>
          </SignupBox>
        </Link>
      </LoginScreen>
    )
  }
}

export default Login
