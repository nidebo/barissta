import React from 'react'
import PropTypes from 'prop-types'

// components
import { AdminContainer, UserContainer, ShopContainer, SummaryContainer } from './../../ui/Admin/Admin';
import AdminEditUser from './AdminEditUser';

class Admin extends React.PureComponent {
  static propTypes = {
    users: PropTypes.object.isRequired,
    shops: PropTypes.object.isRequired,
    summary: PropTypes.object.isRequired
  };

  render() {
    const { users, shops, summary } = this.props;
    return (
      <AdminContainer>
        { Object.keys(summary).length &&
          <div>
            <SummaryContainer>
              <div>Registros Ayer: {summary.signups.yesterday}</div>
              <div>Hoy: {summary.signups.today}</div>
            </SummaryContainer>
            <SummaryContainer>
              <div>Ventas Ayer: {summary.orders.yesterday}</div>
              <div>Hoy: {summary.orders.today}</div>
            </SummaryContainer>
          </div>
        }
        {users.data.map((user, i) => (
          <UserContainer key={i}>
            <div>{user.name}</div>
            <div>{user.email}</div>
            <AdminEditUser user={user} />
          </UserContainer>
        ))}
        {shops.data.map((shop, i) => (
          <ShopContainer key={i}>
            <div>{shop.name}</div>
          </ShopContainer>
        ))}
      </AdminContainer>
    );
  }
}

export default Admin;
