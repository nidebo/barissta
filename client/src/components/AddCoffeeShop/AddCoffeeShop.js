import React from 'react'
import PropTypes from 'prop-types'

// components
import { LoginScreen, Input, InputsContainer, Button, Text, FlashMessage } from './../../ui/Login/Login'
import { SignupContainer } from './../../ui/Signup/Signup'

class AddCoffeeShop extends React.PureComponent {
  static propTypes = {
    addCoffeeShop: PropTypes.func.isRequired,
    newShopState: PropTypes.object.isRequired
  }

  state = {
    name: '',
    address: '',
    email: '',
    image: null,
    errorMessage: '',
    isSuccess: false
  }

  componentWillReceiveProps(nextProps) {
    const { newShopState } = nextProps;

    this.resetState();
    this.setState({ errorMessage: newShopState.error });

    const isSuccess = Object.keys(newShopState.data).length > 0;
    this.setState({ isSuccess });
  }

  resetState = () => {
    this.setState({
      name: '',
      address: '',
      email: '',
      image: null,
      errorMessage: '',
      isSuccess: false
    });
    document.getElementById("uploadImage").value = "";
  }

  onNameChange = (event) => {
    this.setState({ name: event.target.value });
  }

  onAddressChange = (event) => {
    this.setState({ address: event.target.value });
  }

  onEmailChange = (event) => {
    this.setState({ email: event.target.value });
  }

  onFileChange = (event) => {
    this.setState({ image: event.target.files[0] });
  }

  onInputKeyUp = (event) => {
    if (event.key === 'Enter') {
      this.onAddCoffeeShop();
    }
  }

  onAddCoffeeShop = () => {
    const {name, address, email, image } = this.state;
    const { addCoffeeShop } = this.props;
    const coffeeShop = {
      name,
      address,
      email,
      image
    };
    addCoffeeShop(coffeeShop);
  }

  render() {
    const { name, address, email, errorMessage, isSuccess } = this.state;
    return (
      <LoginScreen>
        {errorMessage && <FlashMessage>{errorMessage}</FlashMessage>}
        <SignupContainer>
          <Text>Nuevo Local</Text>
          <InputsContainer>
            <Input type='text' placeholder='nombre' value={name} onChange={this.onNameChange} onKeyUp={this.onInputKeyUp} />
            <Input type='email' placeholder='email' value={email} onChange={this.onEmailChange} onKeyUp={this.onInputKeyUp} />
            <Input type='text' placeholder='dirección' value={address} onChange={this.onAddressChange} onKeyUp={this.onInputKeyUp} />
            <div style={{backgroundColor: 'white'}}>
              <input id="uploadImage" type="file" name="image" accept="image/jpeg" onChange={this.onFileChange} />
            </div>
          </InputsContainer>
          {isSuccess && <div style={{color: "white", marginBottom:"10px"}}>Local añadido correctamente</div>}
          <Button onClick={this.onAddCoffeeShop}>Añadir</Button>
        </SignupContainer>
      </LoginScreen>
    );
  }
}

export default AddCoffeeShop
