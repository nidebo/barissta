import React from 'react'

import { Title } from './../../ui/Home/Home'
import { ContactContainer, Text, ContactDetails } from './../../ui/Contact/Contact'

class ContactBlock extends React.PureComponent {
  render() {
    return (
      <ContactContainer id="contact">
        <Title>Contacto</Title>
        <ContactDetails>
          <Text><i className="fa fa-envelope" style={{"marginRight":"10px"}} aria-hidden="true"></i><a href="mailto:hola@barissta.com">hola@barissta.com</a></Text>
          <Text><i className="fa fa-phone" style={{"marginRight":"10px"}} aria-hidden="true"></i><a href="tel:0034644906930">644 906 930</a></Text>
        </ContactDetails>
      </ContactContainer>
    );
  }
}

export default ContactBlock;
