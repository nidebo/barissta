import React from 'react'
import PropTypes from 'prop-types'

import { ItemBlock, ItemDescription, ItemPrice, ItemDelete } from './../../ui/Item/Item'

import { getFormattedPrice } from './../../utils/helper'

class Item extends React.PureComponent {
  
  static propTypes = {
    item: PropTypes.object.isRequired,
    addItem: PropTypes.func.isRequired,
    removeItem: PropTypes.func.isRequired,
    itemAmountInCart: PropTypes.number.isRequired
  }

  onSelectItem = () => {
    const { addItem, item } = this.props
    addItem(item)
  }

  onRemoveItem = (event) => {
    const { removeItem, item } = this.props
    event.stopPropagation()
    removeItem(item.id)
  }

  render() {
    const { item, itemAmountInCart } = this.props
    
    return (
      <ItemBlock onClick={this.onSelectItem}>
        <ItemDescription>
          <div>{item.name}</div>
          <ItemPrice>{getFormattedPrice(item.price)}</ItemPrice>
        </ItemDescription>
        <div style={{fontSize: "20px"}}>#&nbsp;{itemAmountInCart}</div>
        <ItemDelete onClick={(event) => this.onRemoveItem(event)} className="fa fa-times" aria-hidden="true" />
      </ItemBlock>
    )
  }
}

export default Item
