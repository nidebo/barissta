import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import jwtDecode from 'jwt-decode';
import moment from 'moment';

// ui components
import {
  LoginScreen,
  RecoverPasswordBlock,
  Text,
  Input,
  Button,
  SignupBox,
  FlashMessage
} from './../../ui/Login/Login';

// actions
import { sendRecoverPassword, createNewPassword } from './../../redux/modules/user/actions';

export class RecoverPassword extends React.PureComponent {
  static propTypes = {
    sendRecoverPassword: PropTypes.func.isRequired,
    match: PropTypes.object.isRequired,
  };

  state = {
    email: '',
    password: '',
    isTokenValid: false,
    errorMessage: '',
    successMessage: ''
  };

  onEmailChange = event => {
    this.setState({ email: event.target.value });
  };

  onPasswordChange = event => {
    this.setState({ password: event.target.value });
  };

  onInputKeyUp = event => {
    if (event.key === 'Enter') {
      this.onSubmitRecover();
    }
  };

  onSubmitRecover = () => {
    const { sendRecoverPassword, createNewPassword } = this.props;
    const { email, password } = this.state;
    const { match: { params: { token } } } = this.props;
    if (token) {
      const decodedToken = jwtDecode(token);
      const isTokenValid = moment.unix(decodedToken.exp).isAfter(moment());
      if (isTokenValid) {
        createNewPassword({ token, password })
        this.setState({ password: '' })
      }
    } else {
      sendRecoverPassword(email);
      this.setState({ email: '' })
    }
  };

  showFormAction = () => {
    const { errorMessage, successMessage } = this.state;
    if (errorMessage || !successMessage) {
      return (<Button onClick={this.onSubmitRecover}>Continuar</Button>);
    }
  }

  componentWillMount() {
    const { match } = this.props;
    const { params: { token } } = match;
    if (token) {
      try {
        const decodedToken = jwtDecode(token);
        const isTokenExpired = !moment.unix(decodedToken.exp).isAfter(moment());
        if (isTokenExpired) {
          this.setState({ errorMessage: 'El enlace ha caducado. Solicita uno nuevo.' });
        }
        this.setState({ isTokenValid: !isTokenExpired });
      } catch (error) {
        this.setState({ errorMessage: 'Enlace inválido' });
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    const { responseErrorMessage, responseSuccessMessage } = nextProps;
    this.setState({ errorMessage: responseErrorMessage });
    this.setState({ successMessage: responseSuccessMessage });
    if (responseErrorMessage) {
      window.scrollTo(0, 0)
    }
  }

  render() {
    const { email, password, isTokenValid, errorMessage, successMessage } = this.state;

    return (
      <LoginScreen>
        {errorMessage && <FlashMessage>{errorMessage}</FlashMessage>}
        {successMessage && <FlashMessage success>{successMessage}</FlashMessage>}
        <RecoverPasswordBlock>
          <Text>
            {!isTokenValid ? 'RECUPERA TU CONTRASEÑA' : 'NUEVA CONTRASEÑA'}
          </Text>
          {!isTokenValid
            ? <Input
                style={{marginBottom: '20px', marginTop: '100px'}}
                type="email"
                placeholder="Correo electrónico"
                value={email}
                onChange={this.onEmailChange}
                onKeyUp={this.onInputKeyUp}
                autoFocus
              />
            : <Input
                style={{marginBottom: '20px', marginTop: '100px'}}
                type="password"
                placeholder="Nueva contraseña"
                value={password}
                onChange={this.onPasswordChange}
                onKeyUp={this.onInputKeyUp}
                autoFocus
              />}
          {this.showFormAction()}
        </RecoverPasswordBlock>
        <Link to="/signup" style={{width: '100%'}}>
          <SignupBox>
            <div>¿No tienes cuenta? Regístrate</div>
          </SignupBox>
        </Link>
      </LoginScreen>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    responseErrorMessage: state.userActions.errorMessage,
    responseSuccessMessage: state.userActions.successMessage,
  };
};

const RecoverPasswordContainer = connect(mapStateToProps, {
  sendRecoverPassword,
  createNewPassword,
})(RecoverPassword);

export default RecoverPasswordContainer;
