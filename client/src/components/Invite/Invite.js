import React from 'react';
import PropTypes from 'prop-types'
import MobileDetect from 'mobile-detect';

import { clientHost, fbAppId } from '../../utils/config';

import { InviteContainer, CodeContainer, InviteDescription, Text, EarnedCupsContainer,
         Icon, ShareIcons, WhatsappIcon } from './../../ui/Invite/Invite';

class Invite extends React.PureComponent {
  static propTypes = {
    promoCode: PropTypes.object.isRequired,
  };

  calculateEarnedCups = () => {
    const { promoCode } = this.props;
    if (Object.keys(promoCode).length <= 0) {
      return 0;
    }
    const referredUsers = promoCode.referredUsers.length;
    return parseInt(referredUsers/4, 10);
  }

  componentWillMount() {
    // Load facebook SDK
    window.fbAsyncInit = () => {
      window.FB.init({
        appId      : fbAppId,
        xfbml      : true,
        version    : 'v2.9'
      });
      window.FB.AppEvents.logPageView();
    };

    (function(d, s, id) {
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement(s); js.id = id;
       js.src = "//connect.facebook.net/es_ES/sdk.js";
       fjs.parentNode.insertBefore(js, fjs);
    } (document, 'script', 'facebook-jssdk'));
  }

  onClickShareFacebook = () => {
    const { promoCode } = this.props;
    window.FB.ui({
      method: 'share',
      href: `${clientHost}signup?c=${promoCode.code}`,
      mobile_iframe: true
    }, function(response){});
  }

  isMobileOrTablet() {
    const md = new MobileDetect(window.navigator.userAgent);
    return md.mobile() !== null;
  };

  render() {
    const { promoCode } = this.props;
    const whatsappUrl = `whatsapp://send?text=Hola!%20Te%20invito%20` +
                        `a%20una%20taza%20gratis%20con%20Barissta!%20` +
                        `Regístrate%20en%20${clientHost}signup?c=${promoCode.code}`;

    return (
      <InviteContainer>
        <Text>Tu código de invitación</Text>
        <CodeContainer>{promoCode.code}</CodeContainer>
        <InviteDescription>
          Comparte este código con tus amigos para enviarles una taza
          gratis cuando se registren en Barissta.
          <br/><br/>
          ¡Por cada 4 amigos que invites recibirás una taza gratis!
        </InviteDescription>

          <ShareIcons>
            <div>Comparte</div>
            <i className="fa fa-arrow-right fa-2x" style={{"color":"#000000", "fontSize":"18px"}} aria-hidden="true"></i>
            <Icon className="fa fa-facebook-square fa-2x" onClick={this.onClickShareFacebook} aria-hidden="true"></Icon>
            {this.isMobileOrTablet() && (
              <a href={whatsappUrl}>
                <WhatsappIcon>
                  <Icon className="fa fa-whatsapp fa-2x" aria-hidden="true"></Icon>
                </WhatsappIcon>
              </a>
            )}
          </ShareIcons>

        <Text>Tazas ganadas</Text>
        <EarnedCupsContainer>{this.calculateEarnedCups()}</EarnedCupsContainer>
      </InviteContainer>
    );
  }
}

export default Invite;
