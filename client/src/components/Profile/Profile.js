import React from 'react'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router'
import { Link } from 'react-router-dom'
import _ from 'lodash'

import { ProfileScreen, Greeting, Button, UserMenuItem, CreditNumber,
         ActionSquare, Settings, Actions, Header, SquareTitle, WantCoffeeMessage } from './../../ui/Profile/Profile'
import { Highlighted } from './../../ui/Home/Home'
import Tutorial from './../../components/Tutorial/Tutorial'
import OrderSuccess from './../../components/Order/OrderSuccess'
import loadFacebookSDK from './../../utils/facebookUtil'
import { getDisplayName, getFormattedPrice } from './../../utils/helper'

class Profile extends React.PureComponent {
  static propTypes = {
    User: PropTypes.object.isRequired,
    shouldShowTutorial: PropTypes.bool.isRequired,
    logoutUser: PropTypes.func.isRequired,
    hideTutorialAction: PropTypes.func.isRequired
  }

  state = {
    profileScreenClassName: this.props.shouldShowTutorial ? 'disabled' : 'active',
    facebookConnected: false,
    showOrderSuccess: _.get(this.props, 'location.state.orderSucceeded', false)
  }

  componentWillMount() {
    this.props.history.replace(this.props.location.pathname, undefined)
  }

  setFacebookStatus = () => {
    window.FB.getLoginStatus(response => {
      const isFacebookConnected = response.status === 'connected'
      if (isFacebookConnected) {
        this.setState({ facebookConnected: true })
      }
    })
  }

  componentDidMount() {
    if (window.FB) {
      return this.setFacebookStatus()
    }
    loadFacebookSDK(this.setFacebookStatus)
}

  onLogoutClick = () => {
    const { logoutUser } = this.props
    const { facebookConnected } = this.state
    if (window.FB && facebookConnected) {
      return window.FB.logout(response => {
        logoutUser()
      })
    }
    logoutUser()
  }

  onClickTutorialCheck = () => {
    this.props.hideTutorialAction()
  }

  conditionallyShowTutorial = () => {
    const { shouldShowTutorial } = this.props
    if (shouldShowTutorial) {
      const { User } = this.props
      const displayName = getDisplayName(User.name)
      return (<Tutorial onClickOk={this.onClickTutorialCheck} userName={displayName} />)
    }
  }

  renderSuccessfulOrderScreen = () => {
    const { showOrderSuccess } = this.state
    if (showOrderSuccess) {
      return <OrderSuccess onClickOk={this.onClickOrderSuccess} />
    }
  }

  onClickOrderSuccess = () => {
    this.setState({ showOrderSuccess: false })
  }

  render() {
    const { User } = this.props;
    const displayName = getDisplayName(User.name)
    const formattedCurrentCredit = getFormattedPrice(User.currentCredit)
    const formattedFutureCredit = getFormattedPrice(User.futureCredit)

    return (
      <ProfileScreen>
        {this.renderSuccessfulOrderScreen()}
        {this.conditionallyShowTutorial()}
        <Header>
          <Greeting>¡Hola <Highlighted>{displayName}</Highlighted>!</Greeting>
          <div>
            <div style={{display: 'flex', justifyContent: 'flex-end', width: '300px'}}>
              <WantCoffeeMessage>¿TE APETECE UN&nbsp;<Highlighted>CAFÉ</Highlighted>?</WantCoffeeMessage>
            </div>
            <Link to="/order/select-shop"><Button>PÍDELO</Button></Link>
          </div>
        </Header>
        <Actions>
          <ActionSquare>
            <SquareTitle>Tu saldo este mes:</SquareTitle>
            <CreditNumber><Highlighted>{formattedCurrentCredit}</Highlighted></CreditNumber>
          </ActionSquare>
          <ActionSquare>
            <SquareTitle>El mes que viene tendrás:</SquareTitle>
            <CreditNumber>{formattedFutureCredit}</CreditNumber>
          </ActionSquare>
          {/*<ActionSquare>CÓDIGO</ActionSquare>
          <Link to="/invite"><ActionSquare>Invita</ActionSquare></Link>*/}
        </Actions>
        <Settings>
          {User.isAdmin && <Link to="add-coffee-shop" style={{width: '100%'}}><UserMenuItem>Añade un local</UserMenuItem></Link>}
          {User.isAdmin && <Link to="/admin" style={{width: '100%'}}><UserMenuItem>Admin</UserMenuItem></Link>}
          <Link to="cambiar-password" style={{width: '100%'}}><UserMenuItem>Cambia tu contraseña</UserMenuItem></Link>
          <UserMenuItem logout onClick={this.onLogoutClick}>Cerrar sesión</UserMenuItem>
        </Settings>
      </ProfileScreen>
    );
  }
}

export default withRouter(Profile)
