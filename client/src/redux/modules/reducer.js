// npm pacakges
import { combineReducers } from 'redux'

// reducers & sagas
import adminActions, { adminActionsSaga } from './admin/actions'
import adminSummaryActions, { adminSummaryActionsSaga } from './admin/getSummary'
import adminShopsActions, { adminShopsSaga } from './admin/getShops'
import adminEditUser, { adminEditUserSaga } from './admin/editUser'
import authLogin, { authLoginSaga } from './auth/login'
import authSignup, { authSignupSaga } from './auth/signup'
import userSet from './user/set'
import userGet, { userGetSaga } from './user/get'
import userActions, { userActionsSaga } from './user/actions'
import userAddPaymentMethod, { userAddPaymentMethodSaga } from './user/addPaymentMethod'
import signForPurchaseActions from './user/signForPurchase'
import authLogout, { authLogoutSaga } from './auth/logout'
import shopGet, { shopGetSaga } from './shop/getShop'
import shopsGet, { shopsGetSaga } from './shop/get'
import utilsGoogleMap, { googleMapsSaga } from './utils/googleMap'
import shopSet, { shopSetSaga } from './shop/set'
import inviteActions, { promoCodeSaga } from './invite/actions'
import paymentActions, { paymentSaga } from './payment/actions'
import locationActions, { locationSaga } from './location/actions'
import tutorialActions from './tutorial/actions'
import cart from './cart/cart'

export default combineReducers({
  adminActions,
  adminSummaryActions,
  adminShopsActions,
  adminEditUser,
  authLogin,
  authSignup,
  userSet,
  userGet,
  userActions,
  signForPurchaseActions,
  userAddPaymentMethod,
  authLogout,
  shopGet,
  shopsGet,
  utilsGoogleMap,
  shopSet,
  inviteActions,
  paymentActions,
  locationActions,
  tutorialActions,
  cart
});

export function* rootSaga() {
  yield [
    adminActionsSaga(),
    adminSummaryActionsSaga(),
    adminShopsSaga(),
    adminEditUserSaga(),
    authLoginSaga(),
    authSignupSaga(),
    userGetSaga(),
    userActionsSaga(),
    userAddPaymentMethodSaga(),
    authLogoutSaga(),
    shopGetSaga(),
    shopsGetSaga(),
    googleMapsSaga(),
    shopSetSaga(),
    promoCodeSaga(),
    paymentSaga(),
    locationSaga()
  ];
}
