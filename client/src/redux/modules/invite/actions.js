// npm packages
import { takeLatest, put, call } from 'redux-saga/effects'

// utils
import RestClient from './../../../utils/rest-client'

// actions names
const FETCH_PROMO_CODE = 'INVITE/FETCH_PROMO_CODE';
const FETCH_PROMO_CODE_SUCCESS = 'INVITE/FETCH_PROMO_CODE_SUCCESS';
const FETCH_PROMO_CODE_FAILED = 'INVITE/FETCH_PROMO_CODE_FAILED';

const initialState = {
  promoCode: {
    state: 'READY',
    data: {},
    error: ''
  }
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_PROMO_CODE:
      return { ...state, promoCode: { ...state.promoCode, state: 'PENDING', error: '', data: {} } };
    case FETCH_PROMO_CODE_FAILED:
      return { ...state, promoCode: { ...state.promoCode, state: 'ERROR', error: action.error, data: {} } };
    case FETCH_PROMO_CODE_SUCCESS:
      return { ...state, promoCode: { ...state.promoCode, state: 'READY', error: '', data: action.promoCode } };
    default:
      return state;
  }
}

// action creators
export const fetchPromoCode = () => ({
  type: FETCH_PROMO_CODE
});

const fetchPromoCodeFailed = (error) => ({
  type: FETCH_PROMO_CODE_FAILED,
  error
});

const fetchPromoCodeSuccess = (promoCode) => {
  return {
    type: FETCH_PROMO_CODE_SUCCESS,
    promoCode
  }
};

// worker Saga
function* fetchPromoCodeSaga(action) {
  try {
    const response = yield call(RestClient.get, 'api/referral/code/');
    const promoCode = yield response.json();
    yield put(fetchPromoCodeSuccess(promoCode));
  } catch (e) {
    alert('Error getting promoCode');
    console.error('Error getting promoCode: ', e);
    return yield put(fetchPromoCodeFailed(`Error getting promoCode: ${e}`));
  }
}

// watcher Saga
export function* promoCodeSaga() {
  yield takeLatest(FETCH_PROMO_CODE, fetchPromoCodeSaga);
}
