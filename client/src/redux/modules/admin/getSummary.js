// npm packages
import { takeLatest, put, call } from 'redux-saga/effects'

// utils
import RestClient from './../../../utils/rest-client'

// actions names
const FETCH_SUMMARY = 'ADMIN/FETCH_SUMMARY';
const FETCH_SUMMARY_FAILED = 'ADMIN/FETCH_SUMMARY_FAILED';
const FETCH_SUMMARY_SUCCESS = 'ADMIN/FETCH_SUMMARY_SUCCESS';

const initialState = {
  summary: {
    state: 'READY',
    data: {},
    error: ''
  }
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_SUMMARY:
      return { ...state, summary: { ...state.summary, state: 'PENDING', error: '', data: {} } };
    case FETCH_SUMMARY_FAILED:
      return { ...state, summary: { ...state.summary, state: 'ERROR', error: action.error, data: {} } };
    case FETCH_SUMMARY_SUCCESS:
      return { ...state, summary: { ...state.summary, state: 'READY', error: '', data: {...action.summary } } };
    default:
      return state;
  }
}

// action creators
export const fetchSummary = () => ({
  type: FETCH_SUMMARY
});

const fetchSummaryFailed = (error) => ({
  type: FETCH_SUMMARY_FAILED,
  error
});

const fetchSummarySuccess = (summary) => {
  return {
    type: FETCH_SUMMARY_SUCCESS,
    summary    
  }
};

// worker Saga
function* fetchSummarySaga(action) {
  try {
    const response = yield call(RestClient.get, 'api/admin/summary');
    const summary = yield response.json();
    yield put(fetchSummarySuccess(summary));
  } catch (e) {
    alert('Error getting summary');
    console.error('Error getting summary: ', e);
    return yield put(fetchSummaryFailed(`Error getting summary: ${e}`));
  }
}

// watcher Saga
export function* adminSummaryActionsSaga() {
  yield takeLatest(FETCH_SUMMARY, fetchSummarySaga);
}
