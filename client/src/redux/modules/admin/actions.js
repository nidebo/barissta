// npm packages
import { takeLatest, put, call } from 'redux-saga/effects'

// utils
import RestClient from './../../../utils/rest-client'

// actions names
const FETCH_USERS = 'ADMIN/FETCH_USERS';
const FETCH_USERS_FAILED = 'ADMIN/FETCH_USERS_FAILED';
const FETCH_USERS_SUCCESS = 'ADMIN/FETCH_USERS_SUCCESS';

const initialState = {
  users: {
    state: 'READY',
    data: [],
    error: ''
  }
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_USERS:
      return { ...state, users: { ...state.Users, state: 'PENDING', error: '', data: [] } };
    case FETCH_USERS_FAILED:
      return { ...state, users: { ...state.Users, state: 'ERROR', error: action.error, data: [] } };
    case FETCH_USERS_SUCCESS:
      return { ...state, users: { ...state.Users, state: 'READY', error: '', data: [...action.users] } };
    default:
      return state;
  }
}

// action creators
export const fetchUsers = () => ({
  type: FETCH_USERS
});

const fetchUsersFailed = (error) => ({
  type: FETCH_USERS_FAILED,
  error
});

const fetchUsersSuccess = (users) => {
  return {
    type: FETCH_USERS_SUCCESS,
    users    
  }
};

// worker Saga
function* fetchUsersSaga(action) {
  try {
    const response = yield call(RestClient.get, 'api/admin/users/');
    const users = yield response.json();
    yield put(fetchUsersSuccess(users));
  } catch (e) {
    alert('Error getting users');
    console.error('Error getting users: ', e);
    return yield put(fetchUsersFailed(`Error getting users: ${e}`));
  }
}

// watcher Saga
export function* adminActionsSaga() {
  yield takeLatest(FETCH_USERS, fetchUsersSaga);
}
