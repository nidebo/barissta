import { takeLatest, put, call } from 'redux-saga/effects'

// utils
import RestClient from './../../../utils/rest-client'
import { setTokenCookie } from './../../../utils/manageCookies'

// actions
import { setUser } from '../user/set'
import { showTutorialAction } from '../tutorial/actions'

// actions names
const LOGIN = 'AUTH_LOGIN/LOGIN';
const LOGIN_FAILED = 'AUTH_LOGIN/LOGIN_FAILED';
const LOGIN_SUCCESS = 'AUTH_LOGIN/LOGIN_SUCCESS';

const initialState = {
  isLogging: false,
  loginSuccess: false,
  errorMessage: ''
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case LOGIN:
      return { ...state, isLogging: true, loginSuccess: false, errorMessage: '' };
    case LOGIN_FAILED:
      return { ...state, isLogging: false, loginSuccess: false, errorMessage: action.error };
    case LOGIN_SUCCESS:
      return { ...state, isLogging: false, loginSuccess: true, errorMessage: '' };
    default:
      return state;
  }
}

// action creators
export const login = (options) => {
  return {
  type: LOGIN,
  options
}
}

const loginFailed = (error) => ({
  type: LOGIN_FAILED,
  error
});

const loginSuccess = (token) => {
  return {
    type: LOGIN_SUCCESS
  }
};

// worker saga
function* loginSaga(action) {
  try {
    const { email, password, token } = action.options
    let response
    if (token) {
      response = yield call(RestClient.post, 'api/auth/login/token', { access_token: token })
    } else {
      response = yield call(RestClient.post, 'api/auth/login/email', { email, password })
    }

    if (response.headers && response.headers.get('content-type').indexOf('application/json') >= 0) {
      const responseJSON = yield response.json();
      if (response.status >= 400) {
        console.error('Error login: ', responseJSON);
        return yield put(loginFailed(responseJSON));
      } else {
        const { token, isNewUser } = responseJSON;
        if (isNewUser) {
          yield put(showTutorialAction())
        }
        setTokenCookie(token);
        yield put(setUser(token));
        return yield put(loginSuccess());
      }
    }
    console.error('Error on login request: ', response.statusText);
    return yield put(loginFailed(response.statusText));
  } catch (e) {
    console.error('Error login a user: ', e);
  }
}

// watcher Saga
export function* authLoginSaga() {
  yield takeLatest(LOGIN, loginSaga);
}
