// npm packages
import { takeLatest, put, call } from 'redux-saga/effects'

// utils
import RestClient from './../../../utils/rest-client'
import { setTokenCookie } from './../../../utils/manageCookies'

// actions
import { setUser } from '../user/set'
import { showTutorialAction } from '../tutorial/actions'

// actions names
const SIGNING = 'AUTH_SIGNUP/SIGNING';
const SIGNUP_FAILED = 'AUTH_SIGNUP/SIGNUP_FAILED';
const SIGNUP_SUCCESS = 'AUTH_SIGNUP/SIGNUP_SUCCESS';

const initialState = {
  isSigning: false,
  signupSuccess: false,
  errorMessage: ''
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case SIGNING:
      return { ...state, isSigning: true, signupSuccess: false, errorMessage: '' };
    case SIGNUP_FAILED:
      return { ...state, isSigning: false, signupSuccess: false, errorMessage: action.error };
    case SIGNUP_SUCCESS:
      return { ...state, isSigning: false, signupSuccess: true, errorMessage: '' };
    default:
      return state;
  }
}

// action creators
export const signup = (options) => ({
  type: SIGNING,
  options,
});

const signupFailed = (error) => ({
  type: SIGNUP_FAILED,
  error
});

const signupSuccess = (token) => ({
  type: SIGNUP_SUCCESS,
});

function* signupSaga(action) {
  try {
    const { name, email, password } = action.options;
    const response = yield call(RestClient.post, 'api/auth/signup', { name, email, password });
    if (response.headers && response.headers.get('content-type').indexOf('application/json') >= 0) {
      const token = yield response.json();
      if (response.status >= 400) {
        console.error('Error signup: ', token);
        return yield put(signupFailed(token));
      } else {
        yield put(signupSuccess());
        setTokenCookie(token);
        yield put(showTutorialAction())
        return yield put(setUser(token));
      }
    }
    console.error('Error on signup request: ', response.statusText);
    return yield put(signupFailed(response.statusText));
  } catch (e) {
    console.error('Error signing up a user: ', e);
  }
}

// watcher Saga
export function* authSignupSaga() {
  yield takeLatest(SIGNING, signupSaga);
}
