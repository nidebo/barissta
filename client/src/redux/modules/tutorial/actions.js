// actions names
const SHOW_TUTORIAL = 'TUTORIAL/SHOW_TUTORIAL'
const HIDE_TUTORIAL = 'TUTORIAL/HIDE_TUTORIAL'

const initialState = {
  Tutorial: {
    shouldShow: false
  }
}

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case SHOW_TUTORIAL:
      return {
        Tutorial: {
          shouldShow: true
        }
      };
    case HIDE_TUTORIAL:
      return {
        Tutorial: {
          shouldShow: false
        }
      };
    default:
      return state;
  }
}

// action creators
export const hideTutorialAction = () => ({
  type: HIDE_TUTORIAL
})

export const showTutorialAction = () => ({
  type: SHOW_TUTORIAL
})
