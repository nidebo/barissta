import { put, takeLatest, call, select } from 'redux-saga/effects'

// utils
import RestClient from './../../../utils/rest-client'

import { initCart } from '../cart/cart'
import { getCart } from '../../selectors'

// actions names
const GET_SHOP = 'SHOP_GET/GET_SHOP';
const GET_SHOP_FAILED = 'SHOP_GET/GET_SHOP_FAILED';
const GET_SHOP_SUCCESS = 'SHOP_GET/GET_SHOP_SUCCESS';

const initialState = {
  Shop: {
    state: 'READY',
    data: {},
    error: '',
  },
};

// TODO: add SET_SHOP action, when we don't need to fetch because we have it
export default function reducer(state = initialState, action) {
  switch (action.type) {
    case GET_SHOP:
      return { ...state, Shop: { ...state.Shop, state: 'PENDING', error: '', data: {} } };
    case GET_SHOP_FAILED:
      return { ...state, Shop: { ...state.Shop, state: 'ERROR', error: action.error, data: {} } };
    case GET_SHOP_SUCCESS:
      return { ...state, Shop: { ...state.Shop, state: 'READY', error: '', data: {...action.shop} } };
    default:
      return state;
  }
}

// action creators
export const getShop = (slug) => ({
  type: GET_SHOP,
  slug
});

const getShopFailed = (error) => ({
  type: GET_SHOP_FAILED,
  error,
});

const getShopSuccess = (shop) => ({
  type: GET_SHOP_SUCCESS,
  shop,
});

// worker Sagas
function* getShopSaga(action) {
  try {
    const response = yield call(RestClient.get, `api/shop/${action.slug}`);
    if (response.headers && response.headers.get('content-type').indexOf('application/json') >= 0) {
      const shop = yield response.json();
      if (response.status >= 400) {
        console.error('Error getting shop: ', shop);
        return yield put(getShopFailed(`Error getting shop: ${shop}`));
      } else {
        yield put(getShopSuccess(shop))
        const cart = yield select(getCart)
        if (cart.shop !== shop.slug) {
          yield put(initCart(shop.slug))
        }
        return 
      }
    }
    console.error('Error on getting shop request: ', response.statusText);
    return yield put(getShopFailed(`Error on getting shop request: ${response.statusText}`));
  } catch (e) {
    console.error('Error getting shop: ', e);
    return yield put(getShopFailed(`Error getting shop: ${e}`));
  }
}

export function* shopGetSaga() {
  yield takeLatest(GET_SHOP, getShopSaga);
}
