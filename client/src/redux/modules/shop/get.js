// npm packages
import { put, takeLatest, call } from 'redux-saga/effects';

// utils
import RestClient from './../../../utils/rest-client'

// actions names
const GET_ALL_SHOPS = 'SHOPS_GET/GET_ALL_SHOPS';
const GET_ALL_SHOPS_FAILED = 'SHOPS_GET/GET_ALL_SHOPS_FAILED';
const GET_ALL_SHOPS_SUCCESS = 'SHOPS_GET/GET_ALL_SHOPS_SUCCESS';

const initialState = {
  Shops: {
    state: 'READY',
    data: [],
    error: '',
  },
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case GET_ALL_SHOPS:
      return { ...state, Shops: { ...state.Shops, state: 'PENDING', error: '' } };
    case GET_ALL_SHOPS_FAILED:
      return { ...state, Shops: { ...state.Shops, state: 'ERROR', error: action.error } };
    case GET_ALL_SHOPS_SUCCESS:
      return { ...state, Shops: { ...state.Shops, state: 'READY', error: '', data: [...action.shops] } };
    default:
      return state;
  }
}

// action creators
export const getAllShops = () => ({
  type: GET_ALL_SHOPS,
});

const getAllShopsFailed = (error) => ({
  type: GET_ALL_SHOPS_FAILED,
  error,
});

const getAllShopsSuccess = (shops) => ({
  type: GET_ALL_SHOPS_SUCCESS,
  shops,
});

// worker Sagas
function* getAllShopsSaga(action) {
  try {
    const response = yield call(RestClient.get, 'api/shop');
    if (response.headers && response.headers.get('content-type').indexOf('application/json') >= 0) {
      const shops = yield response.json();
      if (response.status >= 400) {
        console.error('Error getting shops: ', shops);
        return yield put(getAllShopsFailed(`Error getting shops: ${shops}`));
      } else {
        return yield put(getAllShopsSuccess(shops));
      }
    }
    console.error('Error on getting shops request: ', response.statusText);
    return yield put(getAllShopsFailed(`Error on getting shops request: ${response.statusText}`));
  } catch (e) {
    console.error('Error getting shops: ', e);
    return yield put(getAllShopsFailed(`Error getting shops: ${e}`));
  }
}

export function* shopsGetSaga() {
  yield takeLatest(GET_ALL_SHOPS, getAllShopsSaga);
}
