// npm packages
import { put, takeLatest, call } from 'redux-saga/effects'

// utils
import RestClient from './../../../utils/rest-client'

// actions names
const ADD_SHOP = 'SHOP_GET/ADD_SHOP';
const ADD_SHOP_SUCCESS = 'SHOP_GET/ADD_SHOP_SUCCESS';
const ADD_SHOP_FAILED = 'SHOP_GET/ADD_SHOP_FAILED';

const initialState = {
  Shop: {
    state: 'READY',
    data: {},
    error: '',
  },
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case ADD_SHOP:
      return {
        ...state,
        Shop: {
          ...state.Shop,
          state: 'PENDING',
          data: {},
          error: '',
        },
      };
    case ADD_SHOP_FAILED:
      return {
        ...state,
        Shop: {
          ...state.Shop,
          state: 'ERROR',
          data: {},
          error: action.error,
        },
      };
    case ADD_SHOP_SUCCESS:
      return {
        ...state,
        Shop: {
          ...state.Shop,
          state: 'READY',
          data: action.shop,
          error: '',
        },
      };
    default:
      return state;
  }
}

// action creators
export const addShop = (coffeeShop) => ({
  type: ADD_SHOP,
  coffeeShop,
});

const addShopFailed = (error) => ({
  type: ADD_SHOP_FAILED,
  error,
});

const addShopSuccess = (shop) => ({
  type: ADD_SHOP_SUCCESS,
  shop,
});

// worker Saga
function* addShopSaga(action) {
  try {
    const response = yield call(RestClient.postMultipart, 'api/shop', action.coffeeShop);
    if (response.headers && response.headers.get('content-type').indexOf('application/json') >= 0) {
      const shop = yield response.json();
      if (response.status >= 400) {
        console.error('Error getting shop: ', shop);
        return yield put(addShopFailed(shop));
      } else {
        return yield put(addShopSuccess(shop));
      }
    }
    console.error('Error on getting shop request: ', response.statusText);
    return yield put(addShopFailed(response.statusText));
  } catch (e) {
    console.error('Error getting shop: ', e);
    return yield put(addShopFailed(`${e}`));
  }
}

// watcher Saga
export function* shopSetSaga() {
  yield takeLatest(ADD_SHOP, addShopSaga);
}
