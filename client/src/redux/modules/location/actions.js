import { takeLatest, put, call } from 'redux-saga/effects'

// utils
import RestClient from './../../../utils/rest-client'

// actions names
const FETCHING = 'LOCATION/FETCHING';
const FETCH_SUCCESS = 'LOCATION/FETCH_SUCCESS';
const FETCH_FAILED = 'LOCATION/FETCH_FAILED';

const initialState = {
  location: {
    state: 'READY',
    data: {},
    error: ''
  }
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case FETCHING:
      return { ...state, location: { ...state.location, state: 'PENDING', error: '', data: {} } };
    case FETCH_FAILED:
      return { ...state, location: { ...state.location, state: 'ERROR', error: action.error, data: {} } };
    case FETCH_SUCCESS:
      return { ...state, location: { ...state.location, state: 'READY', error: '', data: action.location } };
    default:
      return state;
  }
}

// action creators
export const fetchCurrentLocation = (options) => {
  return {
    type: FETCHING,
    options  
  }
};

const fetchCurrentLocationFailed = (error) => ({
  type: FETCH_FAILED,
  error
});

const fetchCurrentLocationSuccess = (location) => ({
  type: FETCH_SUCCESS,
  location
});

// worker Saga
function* handleFetchCurrentLocationSaga(action) {
  try {
    const response = yield call(RestClient.get, 'api/location/city');

    if (response.status >= 200 && response.status < 300) {
      const city = yield response.json();
      yield put(fetchCurrentLocationSuccess(city));
    } else {
      throw response;
    }
  } catch (e) {
    console.error('Error fetching current location: ', e);
    return yield put(fetchCurrentLocationFailed(`Error fetching current location: ${e}`));
  }
}

// watcher Saga
export function* locationSaga() {
  yield takeLatest(FETCHING, handleFetchCurrentLocationSaga);
}
