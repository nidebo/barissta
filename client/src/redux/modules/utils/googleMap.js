// npm packages
import { put, takeEvery } from 'redux-saga/effects';

const GET_LOCATION = 'UTILS/GET_LOCATION';
const GET_LOCATION_FAILED = 'UTILS/GET_LOCATION_FAILED';
const GET_LOCATION_SUCCESS = 'UTILS/GET_LOCATION_SUCCESS';

const initialState = {
  shopsLocation: [],
  isGettingLocation: false,
  getLocationSuccess: false,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case GET_LOCATION:
      return { ...state, isGettingLocation: true, getLocationSuccess: false };
    case GET_LOCATION_FAILED:
      return { ...state, isGettingLocation: false, getLocationSuccess: false };
    case GET_LOCATION_SUCCESS:
      return { ...state, shopsLocation: [...state.shopsLocation, action.location], isGettingLocation: false, getLocationSuccess: true };
    default:
      return state;
  }
}

// action creators
export const getLocation = (address) => ({
  type: GET_LOCATION,
  address,
});

const getLocationFailed = () => ({
  type: GET_LOCATION_FAILED,
});

const getLocationSuccess = (location) => ({
  type: GET_LOCATION_SUCCESS,
  location,
});

// workers Sagas
function* getLocationSaga(action) {
  try {
    const response = yield fetch(`https://maps.googleapis.com/maps/api/geocode/json?address=${action.address}&key=AIzaSyBgclyYvunHb4ilbondTQ31zRjgiP1R0mU`);
    if (response.headers && response.headers.get('content-type').indexOf('application/json') >= 0) {
      const location = yield response.json();
      if (response.status >= 400) {
        console.error('Error getting location: ', location);
        return yield put(getLocationFailed());
      } else {
        if (location.results.length === 0) {
          return yield put(getLocationFailed());
        } else {
          const {lat, lng} = location.results[0].geometry.location;
          return yield put(getLocationSuccess({ lat, lng }));
        }
      }
    }
  } catch (e) {
    console.error('Error getting location: ', e);
    return yield put(getLocationFailed());
  }
}

// watcher Saga
export function* googleMapsSaga() {
  yield takeEvery(GET_LOCATION, getLocationSaga);
}
