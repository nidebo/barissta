// npm packages
import { takeLatest, put, call } from 'redux-saga/effects';

// utils
import RestClient from './../../../utils/rest-client';
import { setUserCoffees, setUser } from './set';
import { setTokenCookie } from './../../../utils/manageCookies';

// actions names
const DISCOUNT_COFFEE = 'USER_ACTIONS/DISCOUNT_COFFEE';
const DISCOUNT_COFFEE_FAILED = 'USER_ACTIONS/DISCOUNT_COFFEE_FAILED';
const DISCOUNT_COFFEE_SUCCESS = 'USER_ACTIONS/DISCOUNT_COFFEE_SUCCESS';
const CHANGE_PASSWORD = 'USER_ACTIONS/CHANGE_PASSWORD';
const CHANGE_PASSWORD_FAILED = 'USER_ACTIONS/CHANGE_PASSWORD_FAILED';
const CHANGE_PASSWORD_SUCCESS = 'USER_ACTIONS/CHANGE_PASSWORD_SUCCESS';
const SEND_RECOVER_PASSWORD = 'USER_ACTIONS/SEND_RECOVER_PASSWORD';
const SEND_RECOVER_PASSWORD_SUCCESS = 'USER_ACTIONS/SEND_RECOVER_PASSWORD_SUCCESS';
const SEND_RECOVER_PASSWORD_FAILED = 'USER_ACTIONS/SEND_RECOVER_PASSWORD_FAILED';
const CREATE_NEW_PASSWORD = 'USER_ACTIONS/CREATE_NEW_PASSWORD';
const CREATE_NEW_PASSWORD_SUCCESS = 'USER_ACTIONS/CREATE_NEW_PASSWORD_SUCCESS';
const CREATE_NEW_PASSWORD_FAILED = 'USER_ACTIONS/CREATE_NEW_PASSWORD_FAILED';

const initialState = {
  isRequesting: false,
  requestSuccess: false,
  errorMessage: '',
  successMessage: '',
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case DISCOUNT_COFFEE:
    case CHANGE_PASSWORD:
    case SEND_RECOVER_PASSWORD:
    case CREATE_NEW_PASSWORD:
      return {
        ...state,
        isRequesting: true,
        requestSuccess: false,
        errorMessage: '',
        successMessage: '',
      };
    case DISCOUNT_COFFEE_SUCCESS:
    case CHANGE_PASSWORD_SUCCESS:
    case SEND_RECOVER_PASSWORD_SUCCESS:
    case CREATE_NEW_PASSWORD_SUCCESS:
      return {
        ...state,
        isRequesting: false,
        requestSuccess: true,
        errorMessage: '',
        successMessage: action.payload.successMessage,
      };
    case DISCOUNT_COFFEE_FAILED:
    case CHANGE_PASSWORD_FAILED:
    case SEND_RECOVER_PASSWORD_FAILED:
    case CREATE_NEW_PASSWORD_FAILED:
      return {
        ...state,
        isRequesting: false,
        requestSuccess: false,
        errorMessage: action.payload.error,
        successMessage: '',
      };
    default:
      return state;
  }
}

// action creators
export const discountCoffee = shopCode => ({
  type: DISCOUNT_COFFEE,
  shopCode,
});

const discountCoffeeFailed = error => ({
  type: DISCOUNT_COFFEE_FAILED,
  payload: {
    error
  }
});

const discountCoffeeSuccess = successMessage => ({
  type: DISCOUNT_COFFEE_SUCCESS,
  payload: {
    successMessage
  }
});

export const changePassword = options => ({
  type: CHANGE_PASSWORD,
  options,
});

const changePasswordFailed = () => ({
  type: CHANGE_PASSWORD_FAILED,
});

const changePasswordSuccess = () => ({
  type: CHANGE_PASSWORD_SUCCESS,
});

export const sendRecoverPassword = email => ({
  type: SEND_RECOVER_PASSWORD,
  payload: {
    email
  }
});

const sendRecoverPasswordSuccess = successMessage => ({
  type: SEND_RECOVER_PASSWORD_SUCCESS,
  payload: {
    successMessage
  }
});

const sendRecoverPasswordFailed = error => ({
  type: SEND_RECOVER_PASSWORD_FAILED,
  payload: {
    error
  }
});

export const createNewPassword = ({ token, password }) => ({
  type: CREATE_NEW_PASSWORD,
  payload: {
    token,
    password
  }
});

const createNewPasswordSuccess = successMessage => ({
  type: CREATE_NEW_PASSWORD_SUCCESS,
  payload: {
    successMessage
  }
});

const createNewPasswordFailed = error => ({
  type: CREATE_NEW_PASSWORD_FAILED,
  payload: {
    error
  }
});

// worker Saga
function* discountCoffeeSaga(action) {
  try {
    const { shopCode } = action;
    const response = yield call(RestClient.post, 'api/user/discount-coffee', { shopCode });
    if (response.headers && response.headers.get('content-type').indexOf('application/json') >= 0) {
      const token = yield response.json();
      if (response.status >= 400) {
        alert('Ha habido algún error... ¿has introducido bien tu código?');
        console.error('Error discounting coffee: ', token);
        return yield put(discountCoffeeFailed(token));
      } else {
        alert('¡Ya puedes servir la taza!');
        setTokenCookie(token);
        yield put(setUser(token));
        return yield put(discountCoffeeSuccess('Taza validada correctamente'));
      }
    }
    alert('Ha habido algún error... ¿has introducido bien tu código?');
    console.error('Error on discounting coffees request: ', response.statusText);
    return yield put(discountCoffeeFailed(response.statusText));
  } catch (e) {
    alert('Ha habido algún error... ¿has introducido bien tu código?');
    console.error('Error discounting coffees: ', e);
  }
}

function* changePasswordSaga(action) {
  try {
    const { oldPassword, password } = action.options;
    const response = yield call(RestClient.put, 'api/user/change-password', {
      oldPassword,
      newPassword: password,
    });
    if (response.headers && response.headers.get('content-type').indexOf('application/json') >= 0) {
      const token = yield response.json();
      if (response.status >= 400) {
        console.error('Error changing your password: ', token);
        return yield put(changePasswordFailed());
      } else {
        return yield put(changePasswordSuccess('Contraseña cambiada correctamente.'));
      }
    }
    console.error('Error on changing your passwords request: ', response.statusText);
    return yield put(changePasswordFailed());
  } catch (e) {
    console.error('Error changing your passwords: ', e);
  }
}

function* sendRecoverPasswordSaga(action) {
  try {
    const email = action.payload.email;
    const response = yield call(RestClient.post, 'api/user/send-recover-password', { email });
    if (response.status >= 400) {
      yield put(sendRecoverPasswordFailed('No se ha podido enviar el email'));
    } else {
      yield put(
        sendRecoverPasswordSuccess('Email enviado. Accede al enlace en tu correo.')
      );
    }
  } catch (e) {
    console.error('Error sending the recover password email: ', e);
    yield put(sendRecoverPasswordFailed('Error en el envío de email'));
  }
}

function* createNewPasswordSaga(action) {
  try {
    const { token, password } = action.payload;
    const response = yield call(RestClient.post, 'api/user/verify-recover-password', {
      token,
      password,
    });
    if (response.status >= 400) {
      yield put(createNewPasswordFailed('Error en la creación de la nueva contraseña'));
    } else {
      yield put(
        createNewPasswordSuccess(
          'Contraseña cambiada. Ahora puedes acceder a tu cuenta'
        )
      );
    }
  } catch (e) {
    console.error('Error on recovering your password: ', e);
    yield put(createNewPasswordFailed('Error en la creación de la nueva contraseña'));
  }
}

// watcher Saga
export function* userActionsSaga() {
  yield takeLatest(DISCOUNT_COFFEE, discountCoffeeSaga);
  yield takeLatest(CHANGE_PASSWORD, changePasswordSaga);
  yield takeLatest(SEND_RECOVER_PASSWORD, sendRecoverPasswordSaga);
  yield takeLatest(CREATE_NEW_PASSWORD, createNewPasswordSaga);
}
