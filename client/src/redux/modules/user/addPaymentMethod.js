import { takeLatest, put, call } from 'redux-saga/effects'
import { setUser } from '../user/set'

// utils
import RestClient from './../../../utils/rest-client'

// actions names
const ADD_PAYMENT_METHOD = 'PAYMENT_METHOD/ADD';
const ADD_PAYMENT_METHOD_SUCCESS = 'PAYMENT_METHOD/ADD_SUCCESS';
const ADD_PAYMENT_METHOD_FAILED = 'PAYMENT_METHOD/ADD_FAILED';

const initialState = {
  isPending: false,
  didSucceed: false,
  errorMessage: ''
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case ADD_PAYMENT_METHOD:
      return { ...state, isPending: true, didSucceed: false, errorMessage: '' };
    case ADD_PAYMENT_METHOD_FAILED:
      return { ...state, isPending: false, didSucceed: false, errorMessage: action.error };
    case ADD_PAYMENT_METHOD_SUCCESS:
      return { ...state, isPending: false, didSucceed: true, errorMessage: '' };
    default:
      return state;
  }
}

// action creators
export const addPaymentMethod = (options) => {
  return {
    type: ADD_PAYMENT_METHOD,
    options
  }
}

const addPaymentMethodFailed = (error) => ({
  type: ADD_PAYMENT_METHOD_FAILED,
  error
});

const addPaymentMethodSuccess = (token) => {
  return {
    type: ADD_PAYMENT_METHOD_SUCCESS
  }
};

// worker saga
function* addPaymentMethodSaga(action) {
  try {
    const { stripeTokenId } = action.options
    const response = yield call(RestClient.post, 'api/user/payment-method', { stripeTokenId })

    if (response.headers && response.headers.get('content-type').indexOf('application/json') >= 0) {
      const token = yield response.json();
      if (response.status >= 400) {
        console.error('Error adding payment method: ', token);
        return yield put(addPaymentMethodFailed(token));
      } else {
        yield put(setUser(token));
        return yield put(addPaymentMethodSuccess());
      }
    }
    console.error('Error on add payment method request: ', response.statusText);
    return yield put(addPaymentMethodFailed(response.statusText));
  } catch (e) {
    console.error('Error adding payment method: ', e);
  }
}

// watcher Saga
export function* userAddPaymentMethodSaga() {
  yield takeLatest(ADD_PAYMENT_METHOD, addPaymentMethodSaga);
}
