// actions names
const SET_SIGN_FOR_PURCHASE = 'SET_SIGN_FOR_PURCHASE'
const UNSET_SIGN_FOR_PURCHASE = 'UNSET_SIGN_FOR_PURCHASE'

const initialState = {
  isSigningForPurchase: false
}

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case SET_SIGN_FOR_PURCHASE:
      return {
        ...state,
        isSigningForPurchase: true,
        shop: action.shop
      }
    case UNSET_SIGN_FOR_PURCHASE:
      return {
        ...state,
        isSigningForPurchase: false,
        shop: null
      }
    default:
      return state;
  }
}

// action creators
export const setSigningForPurchase = shop => ({
  type: SET_SIGN_FOR_PURCHASE,
  shop
})

export const unsetSigningForPurchase = () => ({
  type: UNSET_SIGN_FOR_PURCHASE
})
