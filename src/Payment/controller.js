import _ from 'underscore'

import userService from '../User/userService'
import orderService from '../Order/orderService'
import emailService from '../Email/emailService'
import authService from '../Auth/authService'
import shopService from '../Shop/shopService'

import { stripeKey } from '../config'
const stripe = require('stripe')(stripeKey)
const paymentService = require('./paymentService').paymentServiceFactory(stripe)

const chargeOrder = async (req, res) => {
  const userToken = req.token
  const { cart } = req.body

  const user = await userService.getUserById(userToken._id)
  if (!user.customer) {
    return res.status(400).json(`User doesn't have a payment method set.`)
  }

  const { items } = cart
  if (_.isEmpty(items)) {
    return res.status(400).json(`There aren't any items in the cart.`)
  }

  const { shop: requestedShopSlug } = cart

  const shop = await shopService.getShopBySlug(requestedShopSlug)
  if (!shop) {
    return res.status(400).json(`Invalid shop for order.`)
  }

  const retrievedItemPromises = Object.keys(items).map(itemId => {
    return shopService.getShopItemById(items[itemId].id)
  })
  const retrievedItems = await Promise.all(retrievedItemPromises)
  const filteredItems = retrievedItems
                          .filter(item => item !== null)
                          // TODO: check that all items belong to the requested SHOP
                          // .filter(.....)

  if (retrievedItems.length !== filteredItems.length) {
    return res.status(400).json(`The order is not valid.`)
  }

  const orderTotalAmount = filteredItems
                            .map(item => item.price * items[item.id].amount)
                            .reduce((sum, itemTotalAmount) => sum + itemTotalAmount, 0)
  const roundedOrderTotalAmount = Math.round(orderTotalAmount * 100) / 100

  const userAvailableCredit = user.currentCredit
  let creditToApply = 0
  if (userAvailableCredit > 0) {
    if (roundedOrderTotalAmount >= userAvailableCredit) {
      creditToApply = userAvailableCredit
    } else {
      creditToApply = roundedOrderTotalAmount
    }
  }

  const discountedAmount = roundedOrderTotalAmount - creditToApply
  const stripeAmount = Math.round(discountedAmount * 100)

  if (stripeAmount > 0 && stripeAmount < 50) {
    return res.status(400).json(`The order does not reach a total minimum.`)
  }

  if (creditToApply > 0) {
    await userService.addCurrentCreditToUser(user._id, -creditToApply)
  }

  const freeOrder = stripeAmount === 0
  let charge = {}
  if (!freeOrder) {
    charge = await paymentService.chargePayment(stripeAmount, { customer: user.customer.id })
    console.log(`charge: ${JSON.stringify(charge)}`)
  }
  if (charge.paid || freeOrder) {
    const orderItems = filteredItems.map(item => {
      return {
        itemId: item.id,
        currentPrice: item.price,
        amount: items[item.id].amount,
        name: item.name
      }
    })
    const { email, _id: userId } = user
    const newOrder = {
      userId,
      shopId: shop._id,
      totalPaid: discountedAmount,
      creditApplied: creditToApply,
      orderItems
    }
    const { createdAt: orderCreatedAt } = await orderService.createOrder(newOrder)
    const userOrderAmount = await orderService.getOrderAmountForUser(userId)
    if (userOrderAmount === 1) {
      emailService.sendEmail('FirstPurchaseEmail', email, `¡Esperamos que hayas disfrutado en ${shop.name}!`, null)
    }
    const orderEmailDetails = {
      userName: user.name,
      orderCreatedAt: orderCreatedAt.toLocaleString('es-ES', { timeZone: 'Europe/Madrid' }),
      orderItems,
      shopName: shop.name
    }
    if (shop.email) {
      emailService.sendEmail('NewOrderShopEmail', shop.email, `¡Habemus pedido!`, orderEmailDetails)
    }
    emailService.sendEmail('NewOrderUserEmail', email, `Tu último pedido en Barissta`, orderEmailDetails)
    await userService.addFutureCreditToUser(userId, Math.round((discountedAmount / 10) * 100) / 100)
    const newToken = await authService.updateUserToken(userId)
    return res.status(200).json(newToken)
  }

  // revert credit if payment failed
  if (creditToApply > 0) {
    await userService.addCurrentCreditToUser(user._id, creditToApply)
  }
  console.error(`Payment failed: ${charge.failure_message}`)
  return res.status(400).json(`Payment failed: ${charge.failure_message}`)
}

export { chargeOrder }
