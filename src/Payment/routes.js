import express from 'express'

import { chargeOrder } from './controller'
import { isAuthenticatedMiddleware } from '../utils'

const router = express.Router()

router.post('/pay', isAuthenticatedMiddleware, chargeOrder)

export default router
