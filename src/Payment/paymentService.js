import _ from 'underscore'

const paymentServiceFactory = (stripe) => {
  const createCustomer = async (user, tokenId) => {
    const customer = await stripe.customers.create({
      email: user.email,
      source: tokenId
    })

    const customerData = customer.sources.data[0]
    const cardData = _.pick(customerData, 'id', 'last4', 'exp_month', 'exp_year', 'brand')
    const pickedCustomerFields = {
      id: customer.id,
      cards: [cardData]
    }
    return pickedCustomerFields
  }

  const chargePayment = async (amount, options) => {
    const charge = await stripe.charges.create({
      amount,
      currency: 'eur',
      description: 'barissta',
      ...options
    })
    return charge
  }

  return {
    createCustomer,
    chargePayment
  }
}

module.exports = { paymentServiceFactory }
