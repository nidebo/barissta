import express from 'express'

import { isAuthenticatedMiddleware } from '../utils'
import {
  changePassword,
  updateToken,
  sendRecoverPassword,
  verifyRecoverPassword,
  addPaymentMethod
} from './controller'

const router = express.Router()

router.post('/payment-method', isAuthenticatedMiddleware, addPaymentMethod)
router.put('/change-password', isAuthenticatedMiddleware, changePassword)
router.get('/update-token', isAuthenticatedMiddleware, updateToken)
router.post('/send-recover-password', sendRecoverPassword)
router.post('/verify-recover-password', verifyRecoverPassword)

export default router
