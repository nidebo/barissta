const getDisplayName = name => {
  if (!name) return ''
  const parsedName = name.trim().split(' ')[0]
  return parsedName.charAt(0).toUpperCase() + parsedName.slice(1)
}

export { getDisplayName }
