import moment from 'moment'

import User from './model/user'

import authService from '../Auth/authService'

const getAllUsers = () => {
  return User.find().cursor()
}

const getUserById = userId => {
  return User.findById(userId)
}

const getUserByEmail = email => {
  return User.findOne({ email })
}

const getUserByAuthProvider = async (id, provider) => {
  const userToken = await authService.getAuthProviderToken(id, provider)
  if (!userToken) return null
  return User.findById(userToken.userId)
}

const createUser = async ({ name, email, password }) => {
  const user = new User({ name, email, password, currentCredit: 0, futureCredit: 0 })
  await user.save()
  return user
}

const addCurrentCreditToUser = (userId, amount) => {
  return User.findByIdAndUpdate(userId, { $inc: { currentCredit: amount } }, { new: true })
}

const addFutureCreditToUser = (userId, amount) => {
  return User.findByIdAndUpdate(userId, { $inc: { futureCredit: amount } }, { new: true })
}

const saveCustomer = (userId, customer) => {
  return User.findOneAndUpdate(
    { _id: userId, customer: { $exists: false } },
    { $set: { customer } },
    { new: true }
  )
}

const getTodaySignups = () => {
  const todayStartDate = moment().startOf('day').toDate()
  return User.find({createdAt: { $gte: todayStartDate }})
}

const getYesterdaySignups = () => {
  const yesterdayStartDate = moment().subtract(1, 'days').startOf('day').toDate()
  const yesterdayEndDate = moment().subtract(1, 'days').endOf('day').toDate()
  return User.find({createdAt: { $gte: yesterdayStartDate, $lt: yesterdayEndDate }})
}

const getUserSignupSummary = async () => {
  const todaySignups = await getTodaySignups()
  const yesterdaySignups = await getYesterdaySignups()
  return { today: todaySignups.length, yesterday: yesterdaySignups.length }
}

export default {
  getAllUsers,
  getUserById,
  getUserByEmail,
  createUser,
  getUserByAuthProvider,
  addCurrentCreditToUser,
  addFutureCreditToUser,
  saveCustomer,
  getUserSignupSummary
}
