import jwt from 'jsonwebtoken'
import passwordHash from 'password-hash'
import fs from 'fs'
import moment from 'moment'

import userService from './../User/userService'
import authService from './../Auth/authService'
import emailService from './../Email/emailService'

import { stripeKey } from '../config'
const stripe = require('stripe')(stripeKey)
const paymentService = require('./../Payment/paymentService').paymentServiceFactory(stripe)

const addPaymentMethod = async (req, res) => {
  const userToken = req.token
  const { stripeTokenId } = req.body

  const user = await userService.getUserById(userToken._id)
  if (!user.customer) {
    const customer = await paymentService.createCustomer(user, stripeTokenId)
    await userService.saveCustomer(user._id, customer)
    const newToken = await authService.updateUserToken(user._id)
    return res.status(200).json(newToken)
  }
  return res.status(400).json('User already has a payment method.')
}

const changePassword = async (req, res) => {
  const token = req.token
  try {
    const oldPassword = req.body.oldPassword
    const user = await userService.getUserById(token._id)
    if (!user) {
      return res.status(400).json('User not found')
    }
    if (!passwordHash.verify(oldPassword, user.password)) {
      return res.status(400).json(`Passwords don't match`)
    }
    const newPassword = passwordHash.generate(req.body.newPassword)
    user.set('password', newPassword)
    await user.save()
    return res.status(200).json('Password successfully updated.')
  } catch (e) {
    console.error('Error updating password: ', e)
    return res.status(400).json('Error updating password.')
  }
}

const updateToken = async (req, res) => {
  const userToken = req.token
  try {
    const updatedToken = await authService.updateUserToken(userToken._id)
    if (!updatedToken) {
      console.log('User not found')
      return res.status(404).json('User not found')
    }
    return res.status(200).json(updatedToken)
  } catch (err) {
    return res.status(500).json('Error getting your updated token auth: ', err)
  }
}

const sendRecoverPassword = async (req, res) => {
  const { email } = req.body
  if (!email) {
    return res.status(400).json('Incorrect request received')
  }

  try {
    const user = await userService.getUserByEmail(email)
    const cert = fs.readFileSync('private.key')
    const token = jwt.sign({ email }, cert, { expiresIn: '5m' })
    user.set('recoverPasswordToken', token)
    await user.save()
    emailService.sendEmail('SendRecoverPassword', email, `Recuperación contraseña Barissta`, { token })
      // TODO: check if email was actually sent
    return res.status(200).json('Recover password email sent successfully')
  } catch (e) {
    console.error('Email is not of an existing user')
    return res.status(400).json('Email is not of an existing user')
  }
}

const verifyRecoverPassword = async (req, res) => {
  const { token, password } = req.body
  try {
    if (!token || !password) {
      return res.status(400).json('Incorrect request received')
    }
    const cert = fs.readFileSync('private.key')
    const verifiedToken = jwt.verify(token, cert)
    const { email, exp } = verifiedToken
    const isTokenValid = moment.unix(exp).isAfter(moment())
    if (!isTokenValid) {
      console.error('Token has expired')
      return res.status(404).json('Token has expired')
    }
    const user = await userService.getUserByEmail(email)
    const recoverPasswordToken = user.recoverPasswordToken
    if (recoverPasswordToken && recoverPasswordToken === token) {
      const newPassword = passwordHash.generate(password)
      user.set('password', newPassword)
      user.set('recoverPasswordToken', '')
      await user.save()
      return res.status(200).json('Password reset successfully')
    } else {
      console.error('Error reseting your password')
      return res.status(404).json('Error reseting your password')
    }
  } catch (e) {
    console.error('Error verifying recovery password')
    return res.status(404).json('Error verifying recovery password')
  }
}

export {
  addPaymentMethod,
  changePassword,
  updateToken,
  sendRecoverPassword,
  verifyRecoverPassword
}
