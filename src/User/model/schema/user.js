import mongoose from 'mongoose'
import CustomerSchema from './customer'

const Schema = mongoose.Schema

const UserSchema = new Schema({
  name: {
    type: String
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String
  },
  currentCredit: {
    type: Number,
    min: 0
  },
  futureCredit: {
    type: Number,
    min: 0
  },
  remainingCoffees: {
    type: Number,
    min: 0
  },
  tookCoffees: [
    {
      _shop: {
        type: Schema.Types.ObjectId,
        ref: 'Shop'
      },
      createdAt: {
        type: Date,
        default: Date.now
      }
    }
  ],
  isAdmin: {
    type: Boolean
  },
  recoverPasswordToken: {
    type: String
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  customer: CustomerSchema
})

export default UserSchema
