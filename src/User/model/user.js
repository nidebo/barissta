import mongoose from 'mongoose'
import UserSchema from './schema/user'

const User = mongoose.model('user', UserSchema)

export default User
