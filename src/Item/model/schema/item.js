import mongoose from 'mongoose'

const Schema = mongoose.Schema

const ItemSchema = new Schema({
  id: {
    type: Number,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  shopId: {
    type: Schema.Types.ObjectId,
    ref: 'Shop'
  },
  type: {
    type: String
  }
})

export default ItemSchema
