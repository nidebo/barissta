import express from 'express'
import { indexHtmlPath } from './config'

const router = express.Router()

router.get('/', (req, res, next) => {
  res
    .status(200)
    .sendFile(indexHtmlPath)
})

export default router
