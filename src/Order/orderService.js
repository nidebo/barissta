import moment from 'moment'
import Order from './model/order'

const createOrder = async ({ userId, shopId, totalPaid, creditApplied, orderItems }) => {
  const order = new Order({ userId, shopId, totalPaid, creditApplied, items: orderItems })
  await order.save()
  return order
}

const getOrderAmountForUser = userId => {
  return Order.find({ userId }).count()
}

const getTodayOrders = () => {
  const todayStartDate = moment().startOf('day').toDate()
  return Order.find({createdAt: { $gte: todayStartDate }})
}

const getYesterdayOrders = () => {
  const yesterdayStartDate = moment().subtract(1, 'days').startOf('day').toDate()
  const yesterdayEndDate = moment().subtract(1, 'days').endOf('day').toDate()
  return Order.find({createdAt: { $gte: yesterdayStartDate, $lt: yesterdayEndDate }})
}

const getOrdersSummary = async () => {
  const todayOrders = await getTodayOrders()
  const yesterdayOrders = await getYesterdayOrders()
  return { today: todayOrders.length, yesterday: yesterdayOrders.length }
}

export default {
  createOrder,
  getOrderAmountForUser,
  getOrdersSummary
}
