import mongoose from 'mongoose'
import OrderSchema from './schema/order'

const Order = mongoose.model('order', OrderSchema)

export default Order
