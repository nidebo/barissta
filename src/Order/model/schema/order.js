import mongoose from 'mongoose'

const Schema = mongoose.Schema

const OrderSchema = new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  totalPaid: {
    type: Number,
    required: true
  },
  creditApplied: {
    type: Number,
    required: true
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  shopId: {
    type: Schema.Types.ObjectId,
    ref: 'Shop'
  },
  items: [
    {
      itemId: {
        type: Number,
        required: true
      },
      amount: {
        type: Number,
        required: true
      },
      currentPrice: {
        type: Number,
        required: true
      }
    }
  ]
})

export default OrderSchema
