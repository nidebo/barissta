import fs from 'fs'
import jwt from 'jsonwebtoken'

const getVerifiedToken = (authHeader) => {
  let verifiedToken
  if (authHeader) {
    const token = authHeader.split(' ')[1]
    const cert = fs.readFileSync('private.key')
    try {
      verifiedToken = jwt.verify(token, cert)
    } catch (e) {
      console.error('JWT malformed')
      return verifiedToken
    }
  }
  return verifiedToken
}

const isAuthenticatedMiddleware = (req, res, next) => {
  const authHeader = req.headers.authorization
  const verifiedToken = getVerifiedToken(authHeader)

  if (verifiedToken) {
    req.token = verifiedToken
    return next()
  }

  return res.status(403).json({
    error: 'Unauthorized action'
  })
}

const isAdminMiddleware = (req, res, next) => {
  const authHeader = req.headers.authorization
  const verifiedToken = getVerifiedToken(authHeader)

  if (verifiedToken && verifiedToken.isAdmin) {
    req.token = verifiedToken
    return next()
  }

  return res.status(403).json({
    error: 'Unauthorized admin action'
  })
}

export { isAuthenticatedMiddleware, isAdminMiddleware }
