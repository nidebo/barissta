import path from 'path'

const PORT = process.env.PORT || 3001
const indexHtmlPath = path.normalize(path.join(__dirname, '../client/build/index.html'))
const staticAssetsPath = path.normalize(path.join(__dirname, '../client/build'))
const apiHost = process.env.API_HOST
const clientHost = process.env.CLIENT_HOST
const stripeKey = process.env.STRIPE_SECRET_KEY
const facebookAppId = process.env.FACEBOOK_APP_ID
const facebookSecretKey = process.env.FACEBOOK_SECRET_KEY

const productionEmailConfig = {
  host: 'smtp.zoho.com',
  port: 465,
  secure: true,
  auth: {
    user: process.env.EMAIL_ACCOUNT,
    pass: process.env.EMAIL_PASSWORD
  }
}

const stagingEmailConfig = {
  host: 'smtp.gmail.com',
  port: 465,
  secure: true,
  auth: {
    user: process.env.EMAIL_ACCOUNT,
    pass: process.env.EMAIL_PASSWORD
  }
}

const developmentEmailConfig = {
  // install mailcatcher: $ gem install mailcatcher
  host: 'localhost',
  port: 1025,
  secure: false,
  auth: {
    user: 'false@example.com'
  }
}

const emailConfigurations = {
  development: developmentEmailConfig,
  staging: stagingEmailConfig,
  production: productionEmailConfig
}

const emailConfig = emailConfigurations[process.env.NODE_ENV]

export {
  PORT,
  indexHtmlPath,
  staticAssetsPath,
  stripeKey,
  apiHost,
  clientHost,
  emailConfig,
  facebookAppId,
  facebookSecretKey
}
