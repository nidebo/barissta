import passport from 'passport'
import FacebookTokenStrategy from 'passport-facebook-token'

import authService from './authService'
import userService from './../User/userService'
import { facebookAppId, facebookSecretKey } from './../config'

const configureFacebookAuthStrategy = () => {
  passport.use(
    new FacebookTokenStrategy({
      clientID: facebookAppId,
      clientSecret: facebookSecretKey
    },
    async (accessToken, refreshToken, profile, done) => {
      const { id, provider } = profile
      const user = await userService.getUserByAuthProvider(id, provider)
      if (user) {
        return done(null, user)
      }

      const { name, emails } = profile
      const email = emails[0].value
      const existingUser = await userService.getUserByEmail(email)
      if (existingUser) {
        await authService.saveAuthProviderToken({ userId: existingUser._id, provider, id, token: accessToken })
        return done(null, existingUser)
      }

      const givenName = name.givenName
      const newUser = await userService.createUser({ name: givenName, email, password: null })
      newUser.isNew = true

      await authService.saveAuthProviderToken({ userId: newUser._id, provider, id, token: accessToken })
      return done(null, newUser)
    })
  )
}

export default configureFacebookAuthStrategy
