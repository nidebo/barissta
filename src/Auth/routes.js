import express from 'express'
import passport from 'passport'

import configureFacebookAuthStrategy from './authStrategy'
import { login, loginWithToken, signup } from './controller'

const router = express.Router()
configureFacebookAuthStrategy()

router.post('/login/email', login)
router.post('/login/token', passport.authenticate('facebook-token', { session: false }), loginWithToken)
router.post('/signup', signup)

export default router
