import express from 'express'

import { getCurrentCity } from './controller'

const router = express.Router()

router.get('/city', getCurrentCity)

export default router
