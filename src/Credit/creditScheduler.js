var path = require('path')
const { fork } = require('child_process')
const CronJob = require('cron').CronJob

const initializeCreditScheduler = () => {
  const job = new CronJob('0 0 1 * *', () => { // to test: switch for '*/1 * * * *'
    const filePath = __dirname + '/processMonthlyCredits.js'
    const compute = fork(path.resolve(filePath))
    console.log(`Processing credit: `, new Date().toUTCString())
  }, null, false, 'Europe/Madrid')
  job.start()
}

export default {
  initializeCreditScheduler
}
