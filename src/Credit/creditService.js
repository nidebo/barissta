import LandingCredit from '../models/LandingCredit'

import userService from '../User/userService'

const shouldUserGetLandingCredit = async (email) => {
  const landingContact = await LandingCredit.findOne({ email, active: false })
  return landingContact !== null
}

const activateLandingCreditToUser = async (user) => {
  const CREDIT_TO_ADD = 5
  let resultUser = user
  const shouldGetCredit = await shouldUserGetLandingCredit(user.email)
  if (shouldGetCredit) {
    await LandingCredit.findOneAndUpdate({ email: user.email }, { active: true })
    resultUser = await userService.addCurrentCreditToUser(user._id, CREDIT_TO_ADD)
  }
  return resultUser
}

export default {
  activateLandingCreditToUser
}
