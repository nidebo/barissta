import mongoose from 'mongoose'
import userService from '../User/userService'
import emailService from '../Email/emailService'
import { getDisplayName } from './../User/util'

mongoose.Promise = global.Promise

const updateUserMonthlyCredit = async userId => {
  try {
    const user = await userService.getUserById(userId)
    const creditToSwap = user.futureCredit
    await userService.addFutureCreditToUser(userId, -creditToSwap)
    const updatedUser = await userService.addCurrentCreditToUser(userId, creditToSwap)
    if (creditToSwap > 0) {
      const emailOptions = {
        name: getDisplayName(updatedUser.name),
        credit: updatedUser.currentCredit
      }
      emailService.sendEmail('NewMonthlyCreditEmail', updatedUser.email, 'Tienes nuevo crédito en Barissta!', emailOptions)
    }
    return updatedUser
  } catch (err) {
    throw new Error(err)
  }
}

mongoose.connect(process.env.MONGODB_URI, { useMongoClient: true })
.then(() => {
  console.log('child Process mongoose connected!')
  const cursor = userService.getAllUsers()
  return cursor.eachAsync(user => {
    return updateUserMonthlyCredit(user._id)
  })
})
.then(() => {
  console.log('bye!')
  process.exit(0)
})
.catch(err => console.error('child Process mongoose error connecting: ', err))
