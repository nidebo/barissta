import nodemailer from 'nodemailer'
import { emailConfig, apiHost, clientHost } from '../config'
import path from 'path'

const EmailTemplate = require('email-templates').EmailTemplate

const transporter = nodemailer.createTransport(emailConfig)

const sendEmail = (template, to, subject, options) => {
  const locals = options || {}
  const templateDir = path.join(__dirname, '../../notifications/templates', template)
  const content = new EmailTemplate(templateDir)

  locals.apiHost = apiHost
  locals.clientHost = clientHost

  content.render(locals, (err, result) => {
    if (err) {
      console.log('sendMail error: ' + JSON.stringify(err))
      return
    }

    const mailOptions = {
      to,
      from: emailConfig.auth.user,
      subject,
      html: result.html
    }

    transporter.sendMail(mailOptions)
    .then((info) => {
      console.log('sendMail success: ' + JSON.stringify(info))
    })
    .catch((err) => {
      console.log('sendMail error: ' + JSON.stringify(err))
    })
  })
}

export default { sendEmail }
