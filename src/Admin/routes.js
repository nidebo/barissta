import express from 'express'

import { getUsers, editUser, getShops, getAdminSummary } from './controller'
import { isAdminMiddleware } from '../utils'

const router = express.Router()

router.get('/summary', isAdminMiddleware, getAdminSummary)
router.get('/users', isAdminMiddleware, getUsers)
router.get('/shops', isAdminMiddleware, getShops)
router.post('/edit/user', isAdminMiddleware, editUser)

export default router
