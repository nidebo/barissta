import User from './../User/model/user'
import Shop from './../Shop/model'

import userService from '../User/userService'
import orderService from '../Order/orderService'

const editUser = async (req, res) => {
  const { email, currentCredit } = req.body
  const updatedUser = await User.findOneAndUpdate(
                         { email },
                         { $set: { currentCredit: currentCredit } },
                         { new: true })
  if (!updatedUser) {
    return res.status(400).json('Email incorrect')
  }
  res.status(200).json('Ok')
}

const getUsers = async (req, res) => {
  const users = await User.find({}).select('-password')
  return res.status(200).json(users)
}

const getShops = async (req, res) => {
  const shops = await Shop.find({})
  return res.status(200).json(shops)
}

const getAdminSummary = (req, res) => {
  return Promise.all([orderService.getOrdersSummary(), userService.getUserSignupSummary()])
  .then(summary => {
    const result = {
      orders: summary[0],
      signups: summary[1]
    }
    return res.status(200).json(result)
  })
  .catch(e => {
    console.error('An error occurred: ', e)
    return res.status(500).json(`An error occurred when trying to getAdminSummary: ${e}`)
  })
}

export { getUsers, getShops, editUser, getAdminSummary }
