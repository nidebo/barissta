import mongoose from 'mongoose'

const Schema = mongoose.Schema

const PromoCodeSchema = new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true,
    unique: true
  },
  code: {
    type: String,
    required: true
  },
  referredUsers: [{
    userId: {
      type: String,
      required: true
    },
    createdAt: {
      type: Date,
      default: Date.now,
      required: true
    }
  }]
})

const PromoCode = mongoose.model('promocode', PromoCodeSchema)

export default PromoCode
