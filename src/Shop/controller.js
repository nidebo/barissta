import path from 'path'
import fetch from 'node-fetch'
import utf8 from 'utf8'
import joi from 'joi'
import cloudinary from 'cloudinary'
import Datauri from 'datauri'

import shopService from '../Shop/shopService'

// models
import Shop from './model'

const getAllShops = async (req, res) => {
  const shops = await shopService.getAllActiveShops()
  return res.status(200).json(shops)
}

const getShopBySlug = async (req, res) => {
  const { slug } = req.params
  const shop = await shopService.getShopBySlug(slug)
  if (!shop) {
    return res.status(404).json('Coffee Shop not found')
  }
  return res.status(200).json(shop)
}

const setLocationCoords = (req, res) => {
  Shop.find()
    .select({ address: 1 })
    .then(shops => {
      shops.forEach(shop => {
        try {
          const utf8Address = utf8.encode(shop.address)
          fetch(`https://maps.googleapis.com/maps/api/geocode/json?address=${utf8Address}&key=AIzaSyBgclyYvunHb4ilbondTQ31zRjgiP1R0mU`)
            .then(response => {
              if (response.headers && response.headers.get('content-type').indexOf('application/json') >= 0) {
                response.json()
                  .then(location => {
                    if (response.status >= 400) {
                      console.error('Error getting location: ', location)
                    } else {
                      if (location.results.length !== 0) {
                        const {lat, lng} = location.results[0].geometry.location
                        shop.coords = { lat, lng }
                        shop.save()
                      }
                    }
                  })
              }
            })
            .catch(e => {
              console.error('Error getting location: ', e)
            })
        } catch (e) {
          console.error('Error getting location: ', e)
        }
      })
    })
    .catch(e => {
      console.error('Error getting the coffee shops: ', e)
      return res.status(404).json('Error getting the coffee shops: ', e)
    })
}

const areNewShopParamsValid = (params) => {
  const schema = joi.object().keys({
    name: joi.string().required(),
    email: joi.string().email().required(),
    address: joi.string().required()
  })

  const { error } = joi.validate({ name: params.name, address: params.address, email: params.email }, schema)
  return error === null
}

const addShop = async (req, res) => {
  const { name, email, address } = req.body
  const coffeeShop = {
    name,
    email,
    address
  }
  if (!areNewShopParamsValid(coffeeShop)) {
    return res.status(400).json('Los datos introducidos no son válidos')
  }
  try {
    const imageDataUri = new Datauri()
    imageDataUri.format(path.extname(req.file.originalname).toString(), req.file.buffer)
    cloudinary.uploader.upload(imageDataUri.content, async (result) => {
      if (!result.public_id) {
        return res.status(400).json(`Error uploading image`)
      }
      coffeeShop.image_url = `${result.public_id}.${result.format}`
      const newCoffeeShop = new Shop(coffeeShop)
      // FIX: store saved coffee shop in variable because it has mongoose pre-save logic
      const newCoffeeShopWithLocation = await newCoffeeShop.save()
      return res.status(200).json(newCoffeeShopWithLocation)
    })
  } catch (e) {
    console.error('Error saving a shop: ', e)
    return res.status(400).json(`${e}`)
  }
}

export { getAllShops, getShopBySlug, setLocationCoords, addShop }
