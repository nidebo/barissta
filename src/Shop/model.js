import mongoose from 'mongoose'
import utf8 from 'utf8'
import fetch from 'node-fetch'
import URLSlugs from 'mongoose-url-slugs'

const Schema = mongoose.Schema

const ShopSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String
  },
  address: {
    type: String
  },
  coords: {
    lat: {
      type: String
    },
    lng: {
      type: String
    }
  },
  openHours: {
    type: [String]
  },
  city: {
    type: String
  },
  active: {
    type: Boolean,
    default: false
  },
  image_url: {
    type: String
  }
})

// slug field
ShopSchema.plugin(URLSlugs('name', { indexSparse: true }))

ShopSchema.pre('save', function (next) {
  try {
    const utf8Address = utf8.encode(this.address)
    fetch(`https://maps.googleapis.com/maps/api/geocode/json?address=${utf8Address}&key=AIzaSyBgclyYvunHb4ilbondTQ31zRjgiP1R0mU`)
      .then(response => {
        if (response.headers && response.headers.get('content-type').indexOf('application/json') >= 0) {
          response.json()
            .then(location => {
              if (response.status >= 400) {
                console.error('Error getting location: ', location)
                return next(new Error('Fallo en la API de Google Maps'))
              } else {
                if (location.results.length !== 0) {
                  const {lat, lng} = location.results[0].geometry.location
                  this.coords = { lat, lng }
                  next()
                }
              }
            })
        }
      })
      .catch(e => {
        console.error('Error getting location: ', e)
        return next(new Error('Fallo en la API de Google Maps'))
      })
  } catch (e) {
    console.error('Error getting location: ', e)
    return next(new Error('Fallo en los datos'))
  }
})

const Shop = mongoose.model('shop', ShopSchema)

Shop.on('index', function (error) {
  if (error) console.log(error.message)
})

export default Shop
