import Shop from '../Shop/model'
import Item from '../Item/model/item'

const getAllActiveShops = () => {
  return Shop.find({ active: true }).select({ _id: 0, idCode: 0, 'items': 0 })
}

const getShopBySlug = async shopSlug => {
  const shopResult = await Shop.findOne({ slug: shopSlug, active: true }).select({ idCode: 0, 'items.amountSold': 0 })
  const shop = shopResult.toObject()
  const items = await Item.find({ shopId: shop._id }).select({ _id: 0 })
  shop.items = items
  return shop
}

const getShopItemById = itemId => {
  return Item.findOne({ id: itemId }).select({ _id: 0 })
}

export default {
  getAllActiveShops,
  getShopBySlug,
  getShopItemById
}
