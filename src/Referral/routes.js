import express from 'express'

import { getPromoCode } from './controller'
import { isAuthenticatedMiddleware } from '../utils'

const router = express.Router()

router.get('/code', isAuthenticatedMiddleware, getPromoCode)

export default router
