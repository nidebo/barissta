import referralService from './referralService'

const getPromoCode = async (req, res) => {
  const token = req.token
  const existingPromoCode = await referralService.getUserPromoCode(token._id)
  if (existingPromoCode) {
    return res.status(200).json(existingPromoCode)
  }
  const createdPromoCode = await referralService.createPromoCode(token._id)
  return res.status(200).json(createdPromoCode)
}

export { getPromoCode }
