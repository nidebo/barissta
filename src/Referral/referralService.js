import PromoCode from '../PromoCode/model'
import userService from '../User/userService'

const isUserReferred = async (userId) => {
  const result = await PromoCode.find({ 'referredUsers.userId': userId.toString() })
  return result.length > 0
}

const shouldAddCoffeeToReferrer = promoCode => {
  const referredTotal = promoCode.referredUsers.length
  if (referredTotal > 0 && referredTotal % 4 === 0) {
    return true
  }
  return false
}

const isPromoCodeValid = async code => {
  const promoCode = await PromoCode.findOne({ code })
  return !!promoCode
}

const createPromoCode = async (userId) => {
  const now = new Date()
  const code = Math.floor(Math.random() * 10) + parseInt(now.getTime()).toString(36).substr(2, 5).toUpperCase()
  const promoCode = new PromoCode({ userId, code, referredUsers: [] })
  try {
    await promoCode.save()
    return promoCode
  } catch (e) {
    if (e.name === 'MongoError' && e.code === 11000) {
      return null
    }
    throw new Error(`Couldn't create promoCode for User: ${e}`)
  }
}

const getUserPromoCode = userId => {
  return PromoCode.findOne({ userId })
}

const processReferredSignup = async (user, code) => {
  const promoCode = await PromoCode.findOne({ code })
  if (promoCode) {
    promoCode.referredUsers.push({ userId: user._id })
    await promoCode.save()
    if (shouldAddCoffeeToReferrer(promoCode)) {
      return userService.addCoffeesToUser(promoCode.userId, 1)
    }
  } else {
    throw new Error(`Promo code should be valid`)
  }
}

export default {
  processReferredSignup,
  isPromoCodeValid,
  isUserReferred,
  createPromoCode,
  getUserPromoCode
}
