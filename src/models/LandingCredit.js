import mongoose from 'mongoose'

const Schema = mongoose.Schema

const LandingCreditSchema = new Schema({
  email: String,
  active: Boolean
})

const LandingCredit = mongoose.model('landingcredit', LandingCreditSchema, 'landingcredit')

export default LandingCredit
