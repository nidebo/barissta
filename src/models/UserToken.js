import mongoose from 'mongoose'

const Schema = mongoose.Schema

const UserTokenSchema = new Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  provider: {
    type: String,
    required: true
  },
  id: {
    type: String,
    required: true
  }
})

const UserToken = mongoose.model('usertoken', UserTokenSchema)

export default UserToken
